#!/usr/bin/env bash
# create build folder and generate make files
mkdir -p build
cd build
cmake ..

# build the code
make

# test cudd
./testcudd

# test the code
./testsolver

# convert all .dot files to .png
for file in *.dot
do
  dot -Tpng $file > "${file%.dot}.png"
done

# run the code
./satsolver $@

