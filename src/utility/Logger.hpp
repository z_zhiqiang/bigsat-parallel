/*
 * Logger.hpp
 *
 *  Created on: Dec 18, 2016
 *      Author: icuzzq
 */

#ifndef UTILITY_LOGGER_HPP_
#define UTILITY_LOGGER_HPP_

#include "../common/BigSATCommon.hpp"

using namespace std;

class Logger
    {
    private:
        ostream& LogStream;
        uint32 ActualLogLevel;
        bool LogStreamIsCout;

    public:
        Logger(uint32 ActualLogLevel);
        Logger(const string& LogFileName, uint32 ActualLogLevel);
        ~Logger();
        void Flush();

        template<typename T> Logger& Log(const T& Obj, uint32 Level);
        template<typename T> Logger& Log(const T& Obj);
        template<typename T> Logger& Log0(const T& Obj);
        template<typename T> Logger& Log1(const T& Obj);
        template<typename T> Logger& Log2(const T& Obj);
        template<typename T> Logger& Log3(const T& Obj);
        template<typename T> Logger& Log4(const T& Obj);
        template<typename T> Logger& Log5(const T& Obj);
        template<typename T> Logger& Log6(const T& Obj);
        template<typename T> Logger& Log7(const T& Obj);
        template<typename T> Logger& Log8(const T& Obj);
        template<typename T> Logger& Log9(const T& Obj);
        template<typename T> Logger& Log10(const T& Obj);

        inline void setLogLevel(uint32 level){
        	ActualLogLevel = level;
        }
    };

    template<typename T> Logger& Logger::Log(const T& Obj, uint32 Level)
    {
        if(ActualLogLevel >= Level) {
            LogStream << Obj; LogStream.flush();
        }
        return *this;
    }

    template<typename T> Logger& Logger::Log(const T& Obj)
    {
        return Log(Obj, 0);
    }

    template<typename T> Logger& Logger::Log0(const T& Obj)
    {
        return Log(Obj, 0);
    }

    template<typename T> Logger& Logger::Log1(const T& Obj)
    {
        return Log(Obj, 1);
    }

    template<typename T> Logger& Logger::Log2(const T& Obj)
    {
        return Log(Obj, 2);
    }

    template<typename T> Logger& Logger::Log3(const T& Obj)
    {
        return Log(Obj, 3);
    }

    template<typename T> Logger& Logger::Log4(const T& Obj)
    {
        return Log(Obj, 4);
    }

    template<typename T> Logger& Logger::Log5(const T& Obj)
    {
        return Log(Obj, 5);
    }

    template<typename T> Logger& Logger::Log6(const T& Obj)
    {
        return Log(Obj, 6);
    }

    template<typename T> Logger& Logger::Log7(const T& Obj)
    {
        return Log(Obj, 7);
    }

    template<typename T> Logger& Logger::Log8(const T& Obj)
    {
        return Log(Obj, 8);
    }

    template<typename T> Logger& Logger::Log9(const T& Obj)
    {
        return Log(Obj, 9);
    }

    template<typename T> Logger& Logger::Log10(const T& Obj)
    {
        return Log(Obj, 10);
    }



#endif /* UTILITY_LOGGER_HPP_ */
