/*
 * Logger.cpp
 *
 *  Created on: Dec 18, 2016
 *      Author: icuzzq
 */

#include "Logger.hpp"

Logger::Logger(const string& LogFileName, uint32 ActualLogLevel) :
		LogStream((LogFileName == "") ? cout : *(new ofstream(LogFileName.c_str()))), ActualLogLevel(ActualLogLevel), LogStreamIsCout(LogFileName == "") {
	// Nothing here
}

Logger::Logger(uint32 ActualLogLevel) :
		LogStream(cout), ActualLogLevel(ActualLogLevel), LogStreamIsCout(true) {
	// Nothing here
}

Logger::~Logger() {
	LogStream.flush();
	if (!LogStreamIsCout) {
		delete (&LogStream);
	}
}

void Logger::Flush() {
	LogStream.flush();
}


