/*
 * ToString.hpp
 *
 *  Created on: Dec 19, 2016
 *      Author: icuzzq
 */

#ifndef UTILITY_TOSTRING_HPP_
#define UTILITY_TOSTRING_HPP_

#include "../common/BigSATCommon.hpp"
#include "../commonimpl/Bucket.hpp"

class ToString{
public:

	static void printOutClauses(const ZBDDNode* root){
		if(root->isONE()){
			std::cout << "[[]]" << std::endl;
		}
		else if(root->isZERO()){
			std::cout << "[]" << std::endl;
		}

		std::vector<std::vector<int>*> paths;
		ZBDDManager::getPaths(root, paths);
		for(auto it = paths.cbegin(); it != paths.cend(); ++it){
			//print out a path
			for(auto init = (*it)->cbegin(); init != (*it)->cend(); ++init){
				std::cout << *init << ", ";
			}
			std::cout << std::endl;

			//clear
			delete *it;
		}
		std::cout << std::endl;
	}

	// This method will print the curernt buckets (for debugging)
	static void printOutBuckets(Bucket** buckets, int numOfVariables){
	    // print out original buckets
		std::cout << "Print buckets..." << std::endl;
		for(int i = 0; i < numOfVariables; ++i){
			Bucket* bucket = buckets[i];
			std::cout << *bucket << std::endl;
		}

		std::cout << "\n\n";
	}

	static void printOutInputs(std::vector<std::vector<int>*> & inputs){
		std::cout << "Inputs: " << std::endl;
//		for(auto& ventry: inputs){
//			for(auto& sentry: *ventry){
//				std::cout << sentry << ", ";
//			}
//			std::cout << std::endl;
//		}

		for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
			for(auto iit = (*it)->cbegin(); iit != (*it)->cend(); ++iit){
				std::cout << *iit << ", ";
			}
			std::cout << std::endl;
		}
	}

	static void printOutOrdering(std::unique_ptr<int[]>& ordering, int& size){
		std::cout << "Variable size: " << size << std::endl;
		for(int i = 0; i < size; ++i){
			std::cout << i << ":" << ordering[i] << std::endl;
		}

	}

	static void printOutClauseVectors(std::vector<Clause*>* vectors_clause, int numOfVariables){
		std::cout << "Clause vectors: " << std::endl;
		for(int i = 0; i < numOfVariables; ++i){
			std::vector<Clause*> vector_c = vectors_clause[i];
			std::cout << i + 1 << ": ";
//			for(auto& entry: vector_c){
//				std::cout << *entry;
//			}
			for(auto it = vector_c.cbegin(); it != vector_c.cend(); ++ it){
				std::cout << **it;
			}
			std::cout << std::endl;
		}
	}

	static void printOutClauseVector(std::vector<Clause*>* vector_clause){
		std::cout << "Clause vector: " << std::endl;
//		for(auto& entry: *vector_clause){
//			std::cout << *entry;
//		}
		for(auto it = vector_clause->cbegin(); it != vector_clause->cend(); ++ it){
			std::cout << **it;
		}
		std::cout << std::endl;
	}

};


#endif /* UTILITY_TOSTRING_HPP_ */
