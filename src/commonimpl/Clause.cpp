
#include "Clause.hpp"


/* Class Constructors & Destructor */

Clause::Clause(const unsigned& size){
	this->size = size;
	this->literals = 0;
}

//Clause::Clause(const std::vector<int>& clause) {
//    // copy data
//    size = clause.size();
//
//    // copy the vector to the array
//    literals = new Literal[size];
//    for (unsigned i = 0; i < size; ++i) {
//        literals[i] = clause[i];
//    }
//}

Clause::Clause(const std::vector<Literal>& clause) {
    // copy data
    size = clause.size();

    // copy the vector to the array
    literals = new Literal[size];
    for (unsigned i = 0; i < size; ++i) {
        literals[i] = clause[i];
    }
}


Clause::Clause(const int literals[], const unsigned& size)
    : size{size} {
    // init the literals pointer
    this->literals = new Literal[size];
    for (unsigned i = 0; i < size; ++i) {
        this->literals[i] = literals[i];
    }
}


Clause::Clause(Literal* literals, const unsigned& size)
    : literals{literals}, size{size} {

}


Clause::~Clause() {
//	std::cout << "Clause destructor called..." << std::endl;
    delete[] literals;
}


/* Class Operators */
Clause& Clause::operator=(const Clause& clause) {
    if (this == &clause) {
        return *this;
    }

    size = clause.size;
    for (unsigned i = 0; i < clause.getSize(); ++i) {
        literals[i] = clause.literals[i];
    }
    return *this;
}


std::ostream& operator<<(std::ostream& strm, const Clause& clause) {
    // print out the fields
    strm << "Clause (key = " << clause.getKey() << ", size = " << clause.size << ") = [";

    // print out the literals
    for (unsigned i = 0; i < (clause.size - 1); ++i) {
        strm << clause.literals[i] << ", ";
    }
    strm << clause.literals[clause.size-1] << "]";

    return strm;
}


/* Public Methods */
// This method will check whether the clause is sorted or not based on the orderings (for debugging)
bool Clause::isSorted(const Clause& clause) {
    // loop through the clause and expected it to be ascending
    for (Clause::const_iterator iter = clause.cbegin(); iter != (clause.cend() - 1); ++iter) {
        if (*iter > *(iter+1)) {
            return false;
        }
    }

    return true;
}

Clause* Clause::doResolution(Clause* other, int key) const{
	std::set<Literal> set;
	for(unsigned i = 0; i < this->size; ++i){
		set.insert(this->literals[i]);
	}
	set.erase(Literal(key));
	set.erase(Literal(-key));
	for(unsigned i = 0; i < other->size; ++i){
		Literal literal = other->literals[i];
		if(set.find(Literal(-literal.getLiteral())) != set.end()){
			return 0;
		}
		set.insert(literal);
	}
	set.erase(Literal(key));
	set.erase(Literal(-key));


	unsigned literalSize = set.size();
	Literal* literals = new Literal[literalSize];
	unsigned index = 0;
	for(auto it = set.cbegin(); it != set.cend(); ++it){
		literals[index++] = *it;
	}

	return new Clause(literals, literalSize);
}
