#ifndef ABSTRACTDATASTRUCTURE_HPP
#define ABSTRACTDATASTRUCTURE_HPP

#include <map>
//#include "zbddimpl/ZBDDManager.hpp"


class AbstractDataStructure {

    public:
        /* Class Constructors & Destructor */
//		AbstractDataStructure();
//		AbstractDataStructure(const int& key);
//		AbstractDataStructure(const Literal& literal);
//		AbstractDataStructure(const AbstractDataStructure& tree);
        virtual ~AbstractDataStructure() {
        	std::cout << "AbstractDataStructure destructor called..." << std::endl;
        };

        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const AbstractDataStructure* data);

        /* Getters & Setters */
        virtual const int& getKey() const = 0;
        virtual unsigned getSize() const = 0;

        /* Public Methods */
        // This method will add a clause to the data structure
        virtual void add(const Clause& clause) = 0;

        // This method will add another data structure to this current data structure
        virtual void add(const AbstractDataStructure* data) = 0;

//        // This method will retrieve all the clauses with given specific literal
//        // in this data structure and remove these clauses from the data structure
//        virtual AbstractDataStructure* retrieve(const Literal& literal) = 0;

//        // This method will do the resolution with a clause from this structure
//        virtual std::map<int,AbstractDataStructure*> doResolution(const Clause& clause) = 0;

        virtual AbstractDataStructure* doResolution(AbstractDataStructure* clauses) = 0;

        // This method will remove everything from this data structure
        virtual void reset() = 0;

//        ZBDDManager* getManager() const;
//
//        const ZBDDNode* getRoot() const;

};


#endif //ABSTRACTDATASTRUCTURE_HPP
