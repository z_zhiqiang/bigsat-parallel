#ifndef BUCKET_HPP
#define BUCKET_HPP

//#include "TrieClauses.hpp"
//#include "../AbstractDataStructure.hpp"
#include "../zbddimpl/ZBDDClauses.hpp"
#include "../common/BigSATCommon.hpp"

// This class represents a bucket with clauses
class Bucket {

    public:
        /* Class Constructors & Destructor */
//        Bucket();
//        Bucket(const int& key);
//        Bucket(const Bucket& bucket);
        Bucket(const unsigned int& key, ZBDDClauses* cls);
//        Bucket(ZBDDClauses* clauses);
        virtual ~Bucket();


        /* Class Operators */
        friend std::ostream& operator<<(std::ostream& strm, const Bucket& bucket);
//        Bucket& operator=(const Bucket& bucket);


        /* Getters & Setters */
        unsigned getSize() const;
        const unsigned int& getKey() const;
        ZBDDClauses* getClauses() const;
        void setClauses(ZBDDClauses* cls);


        /* Public Methods */
        // This method will add this clause to the bucket
        void add(const Clause& clause);

        // This method will add this tree to the bucket
        void add(const ZBDDClauses* tree);

        // This method will add this tree to the bucket
        void add(const ZBDDNode* tree);

        // This method will remove everything in the bucket
        void clean();

        inline bool nonResolution(){
        	return clauses->isEmpty() || clauses->getRoot()->getLow()->isZERO();
        }

        inline bool isUnSAT(){
        	return clauses->getRoot()->getHigh()->isONE() && clauses->getRoot()->getLow()->getHigh()->isONE();
        }


    private:
        /* Declaring Variables */
        unsigned int key;
        ZBDDClauses * clauses;

        std::mutex lock_;
};


/* Inline Functions */
/* Getters & Setters */
inline unsigned Bucket::getSize() const {
    return clauses->getSize();
}


inline const unsigned int& Bucket::getKey() const {
    return key;
}

inline ZBDDClauses* Bucket::getClauses() const{
	return clauses;
}

inline void Bucket::setClauses(ZBDDClauses* cls){
	clauses = cls;
}

#endif // BUCKET_HPP
