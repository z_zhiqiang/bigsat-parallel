/*
 * Bucket_f.hpp
 *
 *  Created on: Feb 7, 2017
 *      Author: icuzzq
 */

#ifndef COMMONIMPL_BUCKET_FLAG_HPP_
#define COMMONIMPL_BUCKET_FLAG_HPP_

#include "Bucket.hpp"

class Bucket_flag : public Bucket {

public:
	Bucket_flag(const unsigned int& key, ZBDDClauses* cls);

	virtual ~Bucket_flag();

	inline void setEndFlag(bool f){
		std::lock_guard<std::mutex> _(lock_flag);
		flag_end = f;
	}

	inline bool isDone() const {
		return flag_end;
	}

private:
	bool flag_end;
	std::mutex lock_flag;

};




#endif /* COMMONIMPL_BUCKET_FLAG_HPP_ */
