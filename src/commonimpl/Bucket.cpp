#include "Bucket.hpp"

/* Class Constructors & Destructor */
//Bucket::Bucket()
//    : key{0}, clauses{} {
//
//}
//
//Bucket::Bucket(const int& key)
//    : key{key}, clauses{0}{
//}
//
//
//Bucket::Bucket(const Bucket& bucket) {
//    // copy the fields
//    key = bucket.key;
//    clauses = bucket.clauses;
//}

Bucket::Bucket(const unsigned int& key, ZBDDClauses* cls)
	: key{key}, clauses{cls}{

}


Bucket::~Bucket(){
//	std::cout << "Bucket destructor called..." << std::endl;
	delete clauses;
}


/* Class Operators */
std::ostream& operator<<(std::ostream& strm, const Bucket& bucket) {
    strm << "Bucket " << bucket.key << std::endl;

    // handle empty bucket
    if (bucket.getSize() == 0) {
        return strm << "Clauses (size = 0):\n" << std::endl;
    }

    strm << "Clauses (size = " << bucket.getSize() << "):\n" << *(bucket.clauses) << std::endl;

    return strm;
}


//Bucket& Bucket::operator=(const Bucket& bucket) {
//    if (this != &bucket) {
//        // assign the fields for this bucket
//        key = bucket.key;
//        clauses = bucket.clauses;
//    }
//
//    return *this;
//}


/* Public Methods */
void Bucket::add(const Clause& clause) {
    // check the clause key
    unsigned int clauseKey = clause.getKey();
    assert((key == clauseKey) && "Adding clause to wrong bucket");

    clauses->add(clause);
}


void Bucket::add(const ZBDDClauses* tree) {
    // check the tree key
    unsigned int treeKey = tree->getKey();
    assert((key == treeKey) && "Adding tree to wrong bucket");

    std::lock_guard<std::mutex> _(lock_);
    clauses->add(tree);
}

void Bucket::add(const ZBDDNode* tree) {
    // check the tree key
    unsigned int treeKey = tree->getKey();
    assert((key == treeKey) && "Adding tree to wrong bucket");

    std::lock_guard<std::mutex> _(lock_);
    clauses->add(tree);
}


void Bucket::clean() {
	clauses->clean();
}
