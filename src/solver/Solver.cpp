#include "../solver/Solver.hpp"

std::mutex lock_add;

/* Class Constructors & Destructor */
Solver::Solver(int numOfVars, Bucket_flag** bucks, Logger* logger, unsigned num_threads): numOfVariables{numOfVars}, buckets{bucks}, logger{logger}, num_threads{num_threads} {

}


Solver::~Solver() {
	logger->Log7("Solver destructor called...\n");
    // delete all the data in buckets
    for (int i = 0; i < numOfVariables; ++i) {
        if (buckets[i]) {
        	buckets[i]->clean();
            delete buckets[i];
        }
    }
}


/* Public Methods */
void Solver::run(ZBDDManager* allManager, const ZBDDNode* all){
	//do resolution for each bucket one by one
	doResolution(allManager, all);
//	doResolution();
}


void refineClauses(const int numOfVariables, Bucket_flag** buckets){
	ZBDDManager zddManager;
	const ZBDDNode* one = ZBDDManager::ZERO;
	for(int i = 0; i < numOfVariables; ++i){
//		if(buckets[i]->isDone()){
//			continue;
//		}
		one = zddManager.unionSubsumFree(one, buckets[i]->getClauses()->getRoot());
		buckets[i]->getClauses()->cleanReset();
	}

	//split resulting clauses into respective buckets
	const ZBDDNode* zdd = one;
	do{
		if(ZBDDManager::isNegative(zdd) && ZBDDManager::isConsecutive(zdd)){
			const ZBDDNode* newlowzdd = new ZBDDNode(zdd->getLow()->getValue(), ZBDDManager::ZERO, zdd->getLow()->getHigh());
			const ZBDDNode* newzdd = new ZBDDNode(zdd->getValue(), newlowzdd, zdd->getHigh());
			buckets[newzdd->getKey() - 1]->add(newzdd);
			delete newzdd;
			delete newlowzdd;
			zdd = zdd->getLow()->getLow();
		}
		else{
			const ZBDDNode* newzdd = new ZBDDNode(zdd->getValue(), ZBDDManager::ZERO, zdd->getHigh());
			buckets[newzdd->getKey() - 1]->add(newzdd);
			delete newzdd;
			zdd = zdd->getLow();
		}

	}
	while(!zdd->isZERO());

	zddManager.cleanNodes();
}

bool isFinished(const int numOfVariables, Bucket_flag** buckets){
	bool flag_fp = true;
	for(int i = 0; i < numOfVariables; ++i){
		if(!buckets[i]->isDone()){
			flag_fp = false;
			break;
		}
	}
	return flag_fp;
}

void Solver::doResolution(){
	for(int length_limit = 3; length_limit <= numOfVariables + 3; length_limit += 3){
		std::cout << length_limit << std::endl;

		for(int i = 0; i < numOfVariables; ++i){
			int bucketID = i;

			if(buckets[bucketID]->isDone()){
				continue;
			}
			else{
				buckets[bucketID]->setEndFlag(true);
			}


			logger->Log4("\nbucket: ").Log4(bucketID + 1).Log4("\n");

			//no resolution for this bucket, i.e., at least one of clauses in this bucket is empty
			if(buckets[bucketID]->nonResolution()){
				continue;
			}

			//empty clause is generated
			if(buckets[bucketID]->isUnSAT()){
				throw UnSATException();
			}

			//do resolution for one bucket
			doResolutionPerBucket_Parallel(bucketID, buckets, length_limit, buckets[bucketID]->getClauses()->getRoot());
		}


		if(isFinished(numOfVariables, buckets)){
			throw SATException();
		}

		//refine clauses via clause subsumption
//		std::cout << "before refine" << std::endl;
		refineClauses(numOfVariables, buckets);
//		std::cout << "end refine" << std::endl;

	}

}

//divide zbdd into multiple small zbdds in order for parallelization
void divideZBDD(const ZBDDNode* zdd, std::vector<const ZBDDNode*>& subs, unsigned num_threads, ZBDDManager& zddManager){
	long num_subs = num_threads * 2;
	long unit_count = zdd->count() / num_subs + 2;

	while(zdd->count() >= unit_count * 2){
		std::pair<const ZBDDNode*, const ZBDDNode*> cut_pair = zddManager.splitZDD(zdd, unit_count);
		subs.push_back(cut_pair.first);
		zdd = cut_pair.second;
	}
	subs.push_back(zdd);
}

//void addClausesPerBucket(Bucket** buckets, int id, ZBDDClauses* zdds){
////	std::lock_guard<std::mutex> _(lock_split);
//	buckets[id]->add(zdds);
//}

void doResolutionAndSplitClauses(const int id, const ZBDDNode* p, const ZBDDNode* q, Bucket_flag** buckets, int length_limit){
	//for debugging
//	std::cout << p->count() << ", " << q->count() << std::endl;

	//do resolution
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* node = zddManager->productSTFree(p, q);
//	std::cout << "product done" << std::endl;

	bool flag_cut = false;
	node = zddManager->createPaths_lengthlimit(flag_cut, length_limit, 0, node);
	if(flag_cut && buckets[id]->isDone()){
		buckets[id]->setEndFlag(false);
	}
//	std::cout << "cut done" << std::endl;

	//if tautological clauses are generated
	if(node->isEmpty()){
		zddManager->cleanNodes();
		delete zddManager;
		return;
	}

	//split resulting clauses into respective buckets
	const ZBDDNode* zdd = node;

	do{
		const ZBDDNode* newzdd;
		if(ZBDDManager::isNegative(zdd) && ZBDDManager::isConsecutive(zdd)){
			const ZBDDNode* newlowzdd = new ZBDDNode(zdd->getLow()->getValue(), ZBDDManager::ZERO, zdd->getLow()->getHigh());
			newzdd = new ZBDDNode(zdd->getValue(), newlowzdd, zdd->getHigh());
			buckets[newzdd->getKey() - 1]->add(newzdd);
			delete newlowzdd;
			zdd = zdd->getLow()->getLow();
		}
		else{
			newzdd = new ZBDDNode(zdd->getValue(), ZBDDManager::ZERO, zdd->getHigh());
			buckets[newzdd->getKey() - 1]->add(newzdd);
			zdd = zdd->getLow();
		}

		if(buckets[newzdd->getKey() - 1]->isDone()){
			buckets[newzdd->getKey() - 1]->setEndFlag(false);
		}
		delete newzdd;
	}
	while(!zdd->isZERO());
//	std::cout << "split done" << std::endl;

	zddManager->cleanNodes();
	delete zddManager;
//	std::cout << "clean done" << std::endl;
}


//------------------------------------------------------------------------------------------------------------------------

const ZBDDNode* getCurrentNode(ZBDDManager* allManager, ZBDDClauses* allClauses, int bucketID, Bucket_flag** buckets){
	const ZBDDNode* newZ = buckets[bucketID]->getClauses()->getRoot();
	newZ = allManager->rebuildZBDD(newZ);
	buckets[bucketID]->getClauses()->cleanReset();

	const ZBDDNode* all = allClauses->getRoot();
	all = allManager->union_zbdd(newZ, all);
	allClauses->setRoot(all);

	const ZBDDNode* zdd = all;
	const ZBDDNode* newzdd = ZBDDManager::ZERO;
	while(!zdd->isZERO()){
		int key = zdd->getKey();
		if(key < bucketID + 1){
			zdd = zdd->getLow();
		}
		else if(key > bucketID + 1){
			break;
		}
		else{
			if(ZBDDManager::isNegative(zdd) && ZBDDManager::isConsecutive(zdd)){
				const ZBDDNode* newlowzdd = allManager->mk(zdd->getLow()->getValue(), ZBDDManager::ZERO, zdd->getLow()->getHigh());
				newzdd = allManager->mk(zdd->getValue(), newlowzdd, zdd->getHigh());
			}
			else{
				newzdd = allManager->mk(zdd->getValue(), ZBDDManager::ZERO, zdd->getHigh());
			}
			break;
		}

	}

//	newzdd = allManager->noSubsumption(newzdd);
	return newzdd;
}


void Solver::doResolution(ZBDDManager* allManager, const ZBDDNode* all){
	ZBDDClauses* allClauses = new ZBDDClauses(all);
	for(int length_limit = 3; length_limit <= numOfVariables + 3; length_limit += 3){
		std::cout << length_limit << std::endl;

		for(int i = 0; i < numOfVariables; ++i){
			int bucketID = i;

			if(buckets[bucketID]->isDone()){
				continue;
			}
			else{
				buckets[bucketID]->setEndFlag(true);
			}

			logger->Log4("\nbucket: ").Log4(bucketID + 1).Log4("\n");

			const ZBDDNode* current = getCurrentNode(allManager, allClauses, bucketID, buckets);

			//no resolution for this bucket, i.e., at least one of clauses in this bucket is empty
			if(current->isEmpty() || current->getLow()->isZERO()){
				continue;
			}

			//empty clause is generated
			if(current->getHigh()->isONE() && current->getLow()->getHigh()->isONE()){
				throw UnSATException();
			}

			//do resolution for one bucket
			doResolutionPerBucket_Parallel(bucketID, buckets, length_limit, current);
//			ToString::printOutClauses(newClauses->getRoot());
		}

		ZBDDManager* newManager = new ZBDDManager();
		allClauses->setRoot(newManager->noSubsumption(allClauses->getRoot()));


		allManager->cleanNodes();
		delete allManager;
		allManager = newManager;

		if(isFinished(numOfVariables, buckets)){
			throw SATException();
		}
	}

}


void Solver::doResolutionPerBucket_Parallel(int id, Bucket_flag** buckets, int length_limit, const ZBDDNode* currentNode){
	ZBDDManager zddManager;
	bool flag_cut = false;
	const ZBDDNode* newRoot = zddManager.createPaths_lengthlimit(flag_cut, length_limit + 1, 0, currentNode);
	if(flag_cut && buckets[id]->isDone()){
		buckets[id]->setEndFlag(false);
	}
	if(newRoot->isEmpty() || newRoot->getLow()->isZERO()){
		zddManager.cleanNodes();
		return;
	}
//	const ZBDDNode* newRoot = clauses->getRoot();

	const ZBDDNode* neg = newRoot->getHigh();
	const ZBDDNode* pos = newRoot->getLow()->getHigh();

	const ZBDDNode* kept = neg;
	const ZBDDNode* divided = pos;
	//divide the bigger zbdd
	if(neg->count() > pos->count()){
		divided = neg;
		kept = pos;
	}

	std::vector<const ZBDDNode*> subs;
	divideZBDD(divided, subs, num_threads, zddManager);
	logger->Log5("split to ").Log5(subs.size()).Log5(" subtasks\n");


	//Create an asio::io_service and a thread_group (thread pool in essence)
	boost::asio::io_service ioService;
	boost::thread_group threadpool;

	//This will start the ioService processing loop. All tasks assigned with ioService.post() will start executing.
	boost::asio::io_service::work* working = new boost::asio::io_service::work(ioService);

	//This will add threads to the thread pool.
	for(unsigned i = 0; i < num_threads; ++i){
		threadpool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
	}

	//This will assign tasks to the thread pool.
	for(std::vector<const ZBDDNode*>::iterator it = subs.begin(); it != subs.end(); ++it){
		ioService.post(boost::bind(doResolutionAndSplitClauses, id, kept, *it, buckets, length_limit));
	}

	//This will stop the ioService processing loop.
	//Any tasks you add behind this point will not execute.
	delete working;

	//Will wait till all the treads in the thread pool are finished with their assigned tasks and 'join' them.
	//Just assume the threads inside the threadpool will be destroyed by this method.
	threadpool.join_all();


	//clean subs
	zddManager.cleanNodes();
}

