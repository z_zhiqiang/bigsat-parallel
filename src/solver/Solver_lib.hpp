/*
 * Solver_cudd.hpp
 *
 *  Created on: Jul 19, 2017
 *      Author: icuzzq
 */

#ifndef SOLVER_SOLVER_CUDD_HPP_
#define SOLVER_SOLVER_CUDD_HPP_

#include "../common/BigSATException.hpp"
#include "../datastructure/AbstractBuckets.hpp"
#include "../utility/Logger.hpp"
#include "../utility/ToString.hpp"

class Solver_lib {
public:
    /* Class Constructors & Destructor */
	Solver_lib(Logger* logger, AbstractBuckets* bucks, unsigned int numOfVars, unsigned int num_threads)
		: logger{logger}, buckets{bucks}, numOfVariables{numOfVars}, num_threads{num_threads} {

	}

	~Solver_lib(){

	}

    /* Public Methods */
    // This method will run the solver
    inline void run(bool incremental_flag){

//		buckets->printToFile("dotOutput.dot");
    	if(incremental_flag)
    		doResoltuion_incremental();
    	else
    		doResolution();
    }


    inline AbstractBuckets* getBuckets(){
    	return buckets;
    }

    inline unsigned int getNumOfVariables(){
    	return numOfVariables;
    }

private:
    /* Declaring Variables */
    Logger* logger;
    AbstractBuckets* buckets;

    unsigned int numOfVariables;
    unsigned int num_threads;



    /* Private Methods */
    // This method will perform resolution
    void doResolution();

    void doResoltuion_incremental();

};



#endif /* SOLVER_SOLVER_CUDD_HPP_ */
