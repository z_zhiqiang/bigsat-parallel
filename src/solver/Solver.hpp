#ifndef SEQUENTIALSOLVER_HPP
#define SEQUENTIALSOLVER_HPP

#include "../common/BigSATException.hpp"
#include "../commonimpl/Bucket_flag.hpp"
#include "../utility/Logger.hpp"
#include "../utility/ToString.hpp"

//struct literal_comparator {
//  bool operator() (const int& lhs, const int& rhs) const {
//	  int abs_l = abs(lhs), abs_r = abs(rhs);
//
//	  if(abs_l < abs_r){
//		  return true;
//	  }
//	  else if(abs_l > abs_r){
//		  return false;
//	  }
//	  else{
//		  return lhs<rhs;
//	  }
//
//  }
//};

// This class represents the parallel solver
class Solver {

    public:
        /* Class Constructors & Destructor */
        Solver(int numOfVars, Bucket_flag** bucks, Logger* logger, unsigned num_threads);
        virtual ~Solver();

        /* Public Methods */
        // This method will run the solver
        void run(ZBDDManager* allManager, const ZBDDNode* all);

        inline Bucket_flag** getBuckets(){
        	return buckets;
        }

        inline int getNumOfVariables(){
        	return numOfVariables;
        }

    private:
        /* Declaring Variables */
        int numOfVariables;
        Bucket_flag** buckets;

        Logger* logger;

        unsigned num_threads;

        /* Private Methods */
        // This method will perform resolution
        void doResolution();
        void doResolution(ZBDDManager* allManager, const ZBDDNode* all);

        // This method will do the resolution with the given bucket in parallel
//        void doResolutionPerBucket_Parallel(int id, Bucket_flag** buckets, int length_limit);
        void doResolutionPerBucket_Parallel(int id, Bucket_flag** buckets, int length_limit, const ZBDDNode* currentNode);

};



#endif // SOLVER_HPP
