/*
 * Solver_cudd.cpp
 *
 *  Created on: Jul 19, 2017
 *      Author: icuzzq
 */

#include "Solver_lib.hpp"


void Solver_lib::doResolution(){
	for(unsigned int i = 0; i < numOfVariables; ++i){
		unsigned int bucketID = i;
		logger->Log4("\nbucket: ").Log4(bucketID + 1).Log4("\n");

		//no resolution for this bucket, i.e., at least one of clauses in this bucket is empty
		if(buckets->is_empty_bucket(bucketID)){
			continue;
		}

		//empty clause is generated
		if(buckets->is_empty_generated(bucketID)){
			throw UnSATException();
		}

		//do resolution for one bucket
		buckets->doResolutionPerBucket_Parallel(bucketID);
	}
	//buckets->print("testOutput.out");
	throw SATException();
}


void Solver_lib::doResoltuion_incremental(){
	for(unsigned int length_limit = 3; length_limit <= numOfVariables + 3; length_limit += 3){
		std::cout << length_limit << std::endl;

		for(unsigned int i = 0; i < numOfVariables; ++i){
			unsigned int bucketID = i;
			logger->Log4("\nbucket: ").Log4(bucketID + 1).Log4("\n");

			if(buckets->isDone(bucketID)){
				continue;
			}
			else{
				buckets->set_done(bucketID, true);
			}

			//no resolution for this bucket, i.e., at least one of clauses in this bucket is empty
			if(buckets->is_empty_bucket(bucketID)){
				continue;
			}

			//empty clause is generated
			if(buckets->is_empty_generated(bucketID)){
				throw UnSATException();
			}

			//do resolution for one bucket
			buckets->doResolutionPerBucket_Parallel(bucketID, length_limit);
		}

		if(buckets->isDone()){
			throw SATException();
		}
	}

}


