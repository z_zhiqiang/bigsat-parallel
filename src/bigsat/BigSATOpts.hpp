/*
 * BigSATOpts.hpp
 *
 *  Created on: Dec 18, 2016
 *      Author: icuzzq
 */

#ifndef SOLVER_BIGSATOPTS_HPP_
#define SOLVER_BIGSATOPTS_HPP_

#include "../common/BigSATCommon.hpp"

#define DEFAULT_LOG_LEVEL (0)
#define DEFAULT_SHORT_LENGTH (0)
#define DEFAULT_NUM_THREADS (boost::thread::hardware_concurrency())

class BigSATOpts{
public:
	BigSATOpts() : StatsLevel(DEFAULT_LOG_LEVEL), LogFileName(""), ShortLength(DEFAULT_SHORT_LENGTH), NumThreads(DEFAULT_NUM_THREADS) {

	}

	~BigSATOpts(){

	}

	int32_t StatsLevel;
	std::string LogFileName;
	unsigned ShortLength;
	unsigned NumThreads;
  std::string inputFileName;
  std::string outputFileName;
  int32_t sylvanLimitSize;
};



#endif /* SOLVER_BIGSATOPTS_HPP_ */
