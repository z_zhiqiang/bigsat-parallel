/*
 * bigsat.cpp
 *
 *  Created on: Dec 18, 2016
 *      Author: icuzzq
 */
#include <boost/program_options.hpp>
#include <sylvan.h>
#include <lace.h>

#include "BigSATOpts.hpp"
#include "../common/BigSATCommon.hpp"
#include "../datastructure/CuddBuckets.hpp"
#include "../preprocess/Preprocessor_GR.hpp"
#include "../preprocess/Preprocessor_lib.hpp"
#include "../solver/Solver.hpp"
#include "../solver/Solver_lib.hpp"

using namespace std;

static inline void ParseOptions(int argc, char* argv[], BigSATOpts& opts, int& solverType) {
  boost::program_options::options_description desc("Usage and Allowed Options");
  boost::program_options::positional_options_description pdesc;
  pdesc.add("input-file", -1);

  desc.add_options()("help", "produce this help message")
    ("input-file,i", boost::program_options::value < string > (&opts.inputFileName)->default_value(""), "BigSAT formatted input file")
    ("verbose,v", boost::program_options::value<int32_t> (&opts.StatsLevel)->default_value(DEFAULT_LOG_LEVEL), "Verbose level")
    ("short-clauses,s", boost::program_options::value < unsigned > (&opts.ShortLength)->default_value(DEFAULT_SHORT_LENGTH), "Length-limit of short clauses for pruning")
    ("number-threads,n", boost::program_options::value < unsigned > (&opts.NumThreads)->default_value(DEFAULT_NUM_THREADS), "Number of threads")
    ("output-file,o", boost::program_options::value < string > (&opts.outputFileName)->default_value(""), "Log output file")
    ("solver-type,t", boost::program_options::value<int32>(&solverType)->default_value(1), "0: zbdd, 1: cudd, 2: sylvan")
    ("sylvan-limit-size,y", boost::program_options::value<int32_t>(&opts.sylvanLimitSize)->default_value(1), "Sylvan size limit");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).positional(pdesc).run(), vm);
  boost::program_options::notify(vm);
  if (vm.count("help") > 0 || argc < 2) {
    cout << desc << endl;
    exit(0);
  }
}

void logResources(Logger* logger, ResourceManager& rm){
  // get timer
  logger->Log0(rm.result()).Log0("\n\n");

  delete logger;
}

void logTimer(Logger* logger, Timer* timer, const std::string& message){
  logger->Log1(message);
  logger->Log0(timer->result()).Log0("\n\n");
}

void cuddSolver(Logger* logger, BigSATOpts opts) { 
  // setup timer
  ResourceManager rm;
  Timer* timer_pp;
  Timer* timer_or;

  AbstractBuckets* buckets = new CuddBuckets();

  try{
    unsigned int numOfVariables;

    // pre-processing
    BOOST_LOG_TRIVIAL(info) << "Start preprocessing...";
    timer_pp = new Timer("pre-processing");
    Preprocessor_lib* pre = new Preprocessor_lib(logger, opts.inputFileName, opts.NumThreads);
    pre->process(numOfVariables, buckets);
    pre->clean();
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");

    // resolution
    BOOST_LOG_TRIVIAL(info) << "Start solving...";
    timer_or = new Timer("ordered-resolution");
    Solver_lib* solver = new Solver_lib(logger, buckets, numOfVariables, opts.NumThreads);
    solver->run(false);
    logTimer(logger, timer_or, "(3) ordered-resolution finished.\n\n");

    // clean logger and buckets
    delete solver; 
    delete logger;
    delete buckets;

  }
  catch(InputFileErrorException& ife){
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");
    logger->Log0("\n\nInvalid file--input error!!!\n\n");
    logResources(logger, rm);
    std::cout << "InF, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << "0, 0, "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(InvalidInputException& ile){
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");
    logger->Log0("\n\nInvalid literal--out of scope!!!\n\n");
    logResources(logger, rm);
    std::cout << "InL, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << "0, 0, "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(UnSATPreprocessException& upe){
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");
    logger->Log0("\n\nUNSAT!!!\n\n");
    logResources(logger, rm);
    std::cout << "UNSAT, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << "0, 0, "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(UnSATException& usate){
    logTimer(logger, timer_or, "(3) ordered-resolution finished.\n\n");
    logger->Log0("\n\nUNSAT!!!\n\n");
    logResources(logger, rm);
    std::cout << "UNSAT, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << timer_or->getWallTimeString() << ", " << timer_or->getCPUTimeString() << ", "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(SATException& sate){
    logTimer(logger, timer_or, "(3) ordered-resolution finished.\n\n");
    logger->Log0("\n\nSAT!!!\n\n");
    logResources(logger, rm);
    std::cout << "SAT, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << timer_or->getWallTimeString() << ", " << timer_or->getCPUTimeString() << ", "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
}

void zbddSolver(Logger* logger, BigSATOpts opts) {
  // setup timer
  ResourceManager rm;
  Timer* timer_pp;
  Timer* timer_or;

  try{
    Bucket_flag** buckets;
    unsigned int numOfVariables;

    ZBDDManager* allManager = new ZBDDManager();
    const ZBDDNode* all = ZBDDManager::ZERO;

    /*Pre-processing*/
    timer_pp = new Timer("pre-processing");
    Preprocessor_GR* pre = new Preprocessor_GR(logger, opts.inputFileName, opts.NumThreads, opts.ShortLength);
    pre->process(numOfVariables, buckets, allManager, all);
    pre->clean();
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");


    /*Resolution*/
    timer_or = new Timer("ordered-resolution");
    Solver* solver = new Solver(numOfVariables, buckets, logger, opts.NumThreads);
    solver->run(allManager, all);
    delete solver;
    logTimer(logger, timer_or, "(3) ordered-resolution finished.\n\n");


    /*Post-processing*/

  }
  catch(InputFileErrorException& ife){
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");
    logger->Log0("\n\nInvalid file--input error!!!\n\n");
    logResources(logger, rm);
    std::cout << "InF, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << "0, 0, "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(InvalidInputException& ile){
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");
    logger->Log0("\n\nInvalid literal--out of scope!!!\n\n");
    logResources(logger, rm);
    std::cout << "InL, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << "0, 0, "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(UnSATPreprocessException& upe){
    logTimer(logger, timer_pp, "(2) pre-processing finished.\n\n");
    logger->Log0("\n\nUNSAT!!!\n\n");
    logResources(logger, rm);
    std::cout << "UNSAT, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << "0, 0, "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(UnSATException& usate){
    logTimer(logger, timer_or, "(3) ordered-resolution finished.\n\n");
    logger->Log0("\n\nUNSAT!!!\n\n");
    logResources(logger, rm);
    std::cout << "UNSAT, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << timer_or->getWallTimeString() << ", " << timer_or->getCPUTimeString() << ", "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
  catch(SATException& sate){
    logTimer(logger, timer_or, "(3) ordered-resolution finished.\n\n");
    logger->Log0("\n\nSAT!!!\n\n");
    logResources(logger, rm);
    std::cout << "SAT, "
      << timer_pp->getWallTimeString() << ", " << timer_pp->getCPUTimeString() << ", "
      << timer_or->getWallTimeString() << ", " << timer_or->getCPUTimeString() << ", "
      << rm.getWallTimeString() << ", " << rm.getCPUTimeString() << ", " << rm.getMemoryString()
      << std::endl;
  }
}

VOID_TASK_2(sylvanSolver, Logger*, logger, BigSATOpts&, opts) {
  // use at most 1Gb, nodes:cache ratio 2:1, initial size 1/32 of max
  sylvan::sylvan_set_limits(opts.sylvanLimitSize * 1024*1024*1024, 1, 5);
  sylvan::sylvan_init_package();
  sylvan::sylvan_init_mtbdd();

  // TODO: ask zhiqiang abt init code

  // close sylvan thread pool
  sylvan::sylvan_stats_report(stdout);
  sylvan::sylvan_quit();
  lace_exit();
}

void initLog() {
  // init boost log
  boost::log::add_common_attributes();
  boost::log::core::get()->add_global_attribute("Scope", boost::log::attributes::named_scope());

  // change boost log format
  // format: [TimeStamp] [ThreadId] [Severity Level] [Scope] log message
  auto boostTimeFormat = boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%H:%M:%S");
  auto boostSeverityFormat = boost::log::expressions::attr<boost::log::trivial::severity_level>("Severity");
  boost::log::formatter logFmt = boost::log::expressions::format("[%1%] [%2%] %3%")
    % boostTimeFormat % boostSeverityFormat % boost::log::expressions::smessage;

  // console sink
  auto consoleSink = boost::log::add_console_log(std::clog);
  consoleSink->set_formatter(logFmt);
}


int main(int argc, char* argv[]) {
  Logger* logger;
  BigSATOpts opts;
  int solverType;

  // parse the arguments
  initLog();
  BOOST_LOG_TRIVIAL(info) << "Parsing arguments...";
  ParseOptions(argc, argv, opts, solverType);
  logger = new Logger(opts.outputFileName, opts.StatsLevel);
  logger->Log1("(1) options-parsing finished.\n\n");

  switch (solverType) {
    case 0:
      BOOST_LOG_TRIVIAL(info) << "Starting zbdd solver...";
      zbddSolver(logger, opts);
      break;
    case 1:
      BOOST_LOG_TRIVIAL(info) << "Starting cudd solver...";
      cuddSolver(logger, opts);
      break;
    default:
      BOOST_LOG_TRIVIAL(info) << "Starting sylvan solver...";
      lace_init(opts.NumThreads, 0); // 32 workers
      lace_startup(0, NULL, NULL);
      LACE_ME;
      CALL(sylvanSolver, logger, opts);
  }

  return 0;
}
