#ifndef CUDD_BUCKETS_HPP_
#define CUDD_BUCKETS_HPP_

#include <memory>
#include "AbstractBuckets.hpp"
#include "cudd.h"


class CuddBuckets : public AbstractBuckets {
  public:
    CuddBuckets();
    virtual ~CuddBuckets();

    /* Public Functions */
    void addClause(Clause& clause);
    void doResolutionPerBucket_Parallel(int bucketId);
    void doResolutionPerBucket_Parallel(int bucketId, unsigned int length_limit);
    bool is_empty_bucket(unsigned int bucketId) const;
    bool is_empty_generated(unsigned int bucketId) const;
    bool isDone(unsigned int bucketId) const;
    void set_done(unsigned int bucketId, bool flag);
    void setVariableCount(unsigned int count);
    void printToFile(const char* filename);
    static void printToFile(const char* filename, DdNode* node, DdManager* manager);
    std::unordered_map<int,std::vector<DdNode*>> findAllNodes(int bucketId) const;

  private:
    DdManager* manager;
    DdNode* root;
    std::unordered_set<int> ids;

    int getLiteralId(int literal);
    int convertBucketId(int bucketId) const;
};


#endif /* CUDD_BUCKETS_HPP_ */
