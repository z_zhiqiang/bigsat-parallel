#include "SylvanBuckets.hpp"

using namespace sylvan;

SylvanBuckets::SylvanBuckets() : bdd{new Bdd(Bdd::bddOne())} {}

SylvanBuckets::~SylvanBuckets() {}

void SylvanBuckets::addClause(Clause& clause) {

}

void SylvanBuckets::doResolutionPerBucket_Parallel(int bucketID) {

}

void SylvanBuckets::doResolutionPerBucket_Parallel(int bucketID, unsigned int length_limit) {

}

void SylvanBuckets::setVariableCount(unsigned int count) {

}
bool SylvanBuckets::is_empty_bucket(unsigned int bucketID) const {

}

bool SylvanBuckets::is_empty_generated(unsigned int bucketID) const {

}

bool SylvanBuckets::isDone(unsigned int bucketID) const {

}

void SylvanBuckets::set_done(unsigned int bucketID, bool flag) {

}

