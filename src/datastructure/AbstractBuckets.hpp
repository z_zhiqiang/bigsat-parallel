#ifndef ABSTRACT_BUCKETS_HPP_
#define ABSTRACT_BUCKETS_HPP_

#include "../commonimpl/Clause.hpp"

class AbstractBuckets{

public:
	AbstractBuckets(){

	}

	virtual ~AbstractBuckets(){

	}

	/*public functions*/
	virtual void addClause(Clause& clause) = 0;
	virtual void doResolutionPerBucket_Parallel(int bucketID) = 0;
	virtual void doResolutionPerBucket_Parallel(int bucketID, unsigned int length_limit) = 0;

	virtual void setVariableCount(unsigned int count) = 0;

	virtual bool is_empty_bucket(unsigned int bucketID) const = 0;

	virtual bool is_empty_generated(unsigned int bucketID) const = 0;

	virtual bool isDone(unsigned int bucketID) const = 0;

	virtual void set_done(unsigned int bucketID, bool flag) = 0;

	bool isDone(){
		for(unsigned int i = 0; i < num_of_variables; ++i){
			if(!isDone(i)){
				return false;
			}
		}
		return true;
	}




protected:

	unsigned int num_of_variables;


};

#endif /* ABSTRACT_BUCKETS_HPP_ */
