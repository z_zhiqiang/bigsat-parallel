#ifndef SYLVAN_BUCKETS_HPP_
#define SYLVAN_BUCKETS_HPP_

#include <sylvan.h>
#include <sylvan_obj.hpp>
#include "AbstractBuckets.hpp"


class SylvanBuckets : public AbstractBuckets {
  public:
    SylvanBuckets();
    virtual ~SylvanBuckets();
  
    void addClause(Clause& clause);
    void doResolutionPerBucket_Parallel(int bucketID);
    void doResolutionPerBucket_Parallel(int bucketID, unsigned int length_limit);
    void setVariableCount(unsigned int count);
    bool is_empty_bucket(unsigned int bucketID) const;
    bool is_empty_generated(unsigned int bucketID) const;
    bool isDone(unsigned int bucketID) const;
    void set_done(unsigned int bucketID, bool flag);    

  private:
    sylvan::Bdd* bdd;
};

#endif /*SYLVAN_BUCKETS_HPP_*/
