#include "CuddBuckets.hpp"

#include <climits>
#include <map>
#include <queue>
#include <string>
#include <limits>

CuddBuckets::CuddBuckets() :
  manager{Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0)},
  root{nullptr} {}

CuddBuckets::~CuddBuckets() {
  if (manager) {
    Cudd_Quit(manager);
    manager = nullptr;
  }

}

void CuddBuckets::addClause(Clause& clause){
  BOOST_LOG_TRIVIAL(info) << "Add clause: " << clause;

  DdNode* tmp;
  DdNode* clauseZdd = Cudd_ReadZddOne(manager, std::numeric_limits<int>::max());
  Cudd_Ref(clauseZdd);
  for (const Literal& l: clause) {
    // convert the literal to the right id
    int id = getLiteralId(l.getLiteral());
    ids.insert(id);
    BOOST_LOG_TRIVIAL(info) << "literal = " << l.getLiteral() << " - id = " << id;

    // add node to the current bdd clause
    clauseZdd = Cudd_zddChange(manager, clauseZdd, id);
  }

  // merge with the current bdd
  if (root) {
    tmp = root;
    root = Cudd_zddUnion(manager, clauseZdd, tmp);
    Cudd_Ref(root);
    Cudd_RecursiveDerefZdd(manager, tmp);
  } else {
    root = clauseZdd;
    Cudd_Ref(root);
  }
  Cudd_RecursiveDerefZdd(manager, clauseZdd);
}

void CuddBuckets::doResolutionPerBucket_Parallel(int bucketId){
  // find all the nodes with bucketId & -bucketId
  int id = convertBucketId(bucketId);
  auto nodes = findAllNodes(bucketId);

  // union all then subgraphs for bucketId & -bucketId
  DdNode* posUnion = nullptr;
  DdNode* negUnion = nullptr;
  DdNode* tmp = nullptr;
  for (auto node : nodes[id]) {
    if (posUnion) {
      tmp = posUnion;
      posUnion = Cudd_zddUnion(manager, tmp, Cudd_T(node)); 
      Cudd_RecursiveDerefZdd(manager, tmp);
    } else {
      posUnion = Cudd_T(node);
    }
    Cudd_Ref(posUnion);
    Cudd_RecursiveDerefZdd(manager, node);
  }
  for (auto node : nodes[id+1]) {
    if (negUnion) {
      tmp = negUnion;
      negUnion = Cudd_zddUnion(manager, tmp, Cudd_T(node));
      Cudd_RecursiveDerefZdd(manager, tmp);
    } else {
      negUnion = Cudd_T(node);
    }
    Cudd_Ref(negUnion);
    Cudd_RecursiveDerefZdd(manager, node);
  }

  // product those subgraphs
  DdNode* resolution = Cudd_zddProduct(manager, posUnion, negUnion);
  Cudd_Ref(resolution);
  Cudd_RecursiveDerefZdd(manager, posUnion);
  Cudd_RecursiveDerefZdd(manager, negUnion);

  // union back to the current bucket
  tmp = root;
  root = Cudd_zddUnion(manager, tmp, resolution);
  Cudd_Ref(root);
  Cudd_RecursiveDerefZdd(manager, tmp);
  Cudd_RecursiveDerefZdd(manager, resolution);
}

void CuddBuckets::doResolutionPerBucket_Parallel(int bucketId, unsigned int length_limit){
  // ignore
}

bool CuddBuckets::is_empty_bucket(unsigned int bucketId) const {
  if (root == nullptr) {
    return false;
  }

  int id = convertBucketId(bucketId); 
  auto result = findAllNodes(bucketId);
  return result[id].empty() || result[id+1].empty();
}

bool CuddBuckets::is_empty_generated(unsigned int bucketId) const {
  if (root == nullptr) {
    return false;
  }

  int id = convertBucketId(bucketId);
  auto result = findAllNodes(bucketId);

  // loop through all the result nodes to see whether they point to 1
  // Note: this is bad code...but whatever lol
  bool isOnlyNode = true;
  for (DdNode* node : result[id]) {
    if (Cudd_IsNonConstant(Cudd_T(node))) {
      isOnlyNode = false;
      break;
    }
  }
  for (DdNode* node : result[id+1]) {
    if (Cudd_IsNonConstant(Cudd_T(node))) {
      isOnlyNode = false;
      break;
    }
  }

  return !result[id].empty() && !result[id+1].empty() && isOnlyNode;
}

bool CuddBuckets::isDone(unsigned int bucketId) const {
  // ignore
  return true;
}

void CuddBuckets::set_done(unsigned int bucketId, bool flag) {
 // ignore
}

void CuddBuckets::setVariableCount(unsigned int count) {
 // ignore
}


void CuddBuckets::printToFile(const char* filename) {
  CuddBuckets::printToFile(filename, root, manager);
}

/* static */ void CuddBuckets::printToFile(const char* filename, DdNode* node, DdManager* manager) {
  DdNode** toPrint = new DdNode*[1];
  toPrint[0] = node;
  FILE* file = fopen(filename, "w");
  Cudd_zddDumpDot(manager, 1, toPrint, NULL, NULL, file);
  fclose(file);
  delete[] toPrint;
}

// This function will traverse the whole graph to find all the nodes 
// with the given bucketId in bfs style
// Ex: bucket 1 will find node 1 and 2; bucket 2 will find node 3 and 4
std::unordered_map<int,std::vector<DdNode*>> CuddBuckets::findAllNodes(int bucketId) const {
  int id = convertBucketId(bucketId);
  std::unordered_map<int,std::vector<DdNode*>> result;
  result[id] = std::vector<DdNode*>();
  result[id+1] = std::vector<DdNode*>();

  // traverse the graph
  std::queue<DdNode*> nodeQ;
  nodeQ.push(root);
  while (!nodeQ.empty()) {
    DdNode* node = nodeQ.front();
    nodeQ.pop();

    // check node index
    if (Cudd_NodeReadIndex(node) == id) {
      result[id].push_back(node);
    } else if (Cudd_NodeReadIndex(node) == id + 1) {
      result[id+1].push_back(node);
    }

    // push then/else subgraphs
    DdNode* thenNode = Cudd_T(node);
    DdNode* elseNode = Cudd_E(node);
    if ((thenNode != nullptr) && (Cudd_IsNonConstant(thenNode))
        && (Cudd_NodeReadIndex(thenNode) <= id + 1)) {
      nodeQ.push(thenNode);
    }
    if ((elseNode != nullptr) && (Cudd_IsNonConstant(elseNode))
        && (Cudd_NodeReadIndex(elseNode) <= id + 1)) {
      nodeQ.push(elseNode);
    }
  }
  
  return result;
}

/* Private Functions */
// This function will map 1 to 1; -1 to 2; 2 to 3; -2 to 4; etc
int CuddBuckets::getLiteralId(int literal) {
  return (literal < 0) ? (-2 * literal) : (2 * literal - 1);
}

// This function will convert the bucket id to the read id
// Ex: bucket 1 to 1; bucket 2 to 3; bucket 3 to 5; etc
int CuddBuckets::convertBucketId(int bucketId) const {
  return std::abs(bucketId) * 2 - 1;
}
