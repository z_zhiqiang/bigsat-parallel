/*
 * Preprocessor_cudd.cpp
 *
 *  Created on: Jul 19, 2017
 *      Author: icuzzq
 */

#include "Preprocessor_lib.hpp"

void Preprocessor_lib::renameCNF(AbstractBuckets* buckets){
	for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
		std::vector<int>* vector_literals = *it;

		unsigned literalSize = vector_literals->size();
		Literal* literals = new Literal[literalSize];
		unsigned index = 0;
		for(auto iit = vector_literals->cbegin(); iit != vector_literals->cend(); ++iit){
			int literal = *iit;
			int abs_literal = abs(literal);

			int abs_new = staticOrdering[abs_literal - 1];
			int new_literal = (literal > 0) ? abs_new : -abs_new;
			literals[index++] = Literal{new_literal};
		}

//		//for debugging
//		for(unsigned i = 0; i < literalSize; ++i){
//			std::cout << literals[i] << ", ";
//		}
//		std::cout << std::endl;

		// sort the literals
		std::sort(literals, literals + literalSize);

//		//for debugging
//		for(unsigned i = 0; i < literalSize; ++i){
//			std::cout << literals[i] << ", ";
//		}
//		std::cout << std::endl << std::endl;

        // instantiate the clause
        Clause clause{literals, literalSize};

        // add the clause to the right bucket
        buckets->addClause(clause);
	}
}



