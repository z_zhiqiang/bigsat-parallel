/*
 * Preprocessor_GR.cpp
 *
 *  Created on: Dec 27, 2016
 *      Author: icuzzq
 */
#include "Preprocessor_GR.hpp"

std::mutex lock_add_clause;
std::mutex lock_add_clauses;

//Preprocessor_GR::Preprocessor_GR(Logger* logger, const std::string& inputfileName, unsigned int num_threads, unsigned int length_thres)
//	: Preprocessor(logger, inputfileName, num_threads) {
////	this->inputFile = inputfileName;
////	this->logger = logger;
////	this->num_threads = num_threads;
//	this->length_threshold = length_thres;
//
//	this->clauses = new ZBDDClauses();
//}
//
//Preprocessor_GR::~Preprocessor_GR(){
//}

void Preprocessor_GR::clean(){
	//clean short clauses
	clauses->clean();
	delete clauses;

	//deallocate inputs
//	for (auto vector_literals : inputs) {
//		delete vector_literals;
//	}
	for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
		delete *it;
	}

	delete [] staticOrdering;
}


static bool cleanVectorClauseGarbage(const std::vector<int>* clause){
	delete clause;
	return true;
}

static bool sortLiterals(int r, int l){
    int abs1 = std::abs(r);
    int abs2 = std::abs(l);
    return (abs1 == abs2) ? (r < l) : (abs1 < abs2);
}

//at the beginning, assign clauses accordingly
void Preprocessor_GR::assignClausesInit(std::vector<std::vector<int>*>* vectors_pos_new, std::vector<std::vector<int>*>* vectors_neg_new, ZBDDClauses* clauses_old){
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* node_clauses = ZBDDManager::ZERO;

	for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
		std::vector<int>* vector_literals = *it;
		if(vector_literals->size() <= length_threshold + 1){
			for(auto iit = vector_literals->cbegin(); iit != vector_literals->cend(); ++iit){
				int literal = *iit;
				if(literal > 0){
					vectors_pos_new[literal - 1].push_back(vector_literals);
				}
				else{
					vectors_neg_new[-literal - 1].push_back(vector_literals);
				}
			}

			//sort the literals
			std::sort(vector_literals->begin(), vector_literals->end(), sortLiterals);

			//add into zbdd clauses for subsumption elimination
//			clauses_old->add(*vector_literals);
			const ZBDDNode* path = zddManager->createPath(*vector_literals);
			node_clauses = zddManager->unionSubsumFree(node_clauses, path);

//			//for debugging
//			std::cout << *clause << std::endl;
		}
	}

	clauses_old->add(node_clauses);

	zddManager->cleanNodes();
	delete zddManager;
}

//assign clauses accordingly and free clause_vector
void assignClauses(std::vector<std::vector<int>*>& diff_vectors, std::vector<std::vector<int>*>* vectors_pos, std::vector<std::vector<int>*>* vectors_neg){
	for(auto it_diff = diff_vectors.cbegin(); it_diff != diff_vectors.cend(); ++it_diff){
		std::vector<int>* clause_vector = *it_diff;

		for(auto iit = clause_vector->cbegin(); iit != clause_vector->cend(); ++iit){
			int literal = *iit;
			if(literal > 0){
				vectors_pos[literal - 1].push_back(clause_vector);
			}
			else{
				vectors_neg[-literal - 1].push_back(clause_vector);
			}
		}
	}
}


const ZBDDNode* buildClauses(int i, bool flag, std::vector<std::vector<int>*>* vectors, ZBDDManager* zddManager){
	int excluded_literal = i + 1;
	if(!flag){
		excluded_literal = - excluded_literal;
	}
	const ZBDDNode* node_clauses = ZBDDManager::ZERO;
	for(auto it = vectors[i].cbegin(); it != vectors[i].cend(); ++it){
		std::vector<int>* clause = *it;
		const ZBDDNode* p = zddManager->createPath(*clause, excluded_literal);
		node_clauses = zddManager->unionSubsumFree(node_clauses, p);
	}

//	//for debugging only
//	std::cout << "build clauses for " << i + 1 << " " << flag << "\n";
//	ToString::printOutClauses(node_clauses);
	return node_clauses;
}

const ZBDDNode* productFilter(const ZBDDNode* pos_new, const ZBDDNode* neg_new, ZBDDManager* zddManager, unsigned length_threshold){
	const ZBDDNode* pos_new_neg_new = zddManager->productSTFree(pos_new, neg_new);
	bool flag = false;
	const ZBDDNode* pos_new_neg_new_node = zddManager->createPaths_lengthlimit(flag, length_threshold, 0, pos_new_neg_new);
//	const ZBDDNode* pos_new_neg_new_node = ZBDDManager::ZERO;
//	std::vector<std::vector<int>*> pos_new_neg_new_vectors;
//	ZBDDManager::getPaths_lengthlimit(length_threshold, pos_new_neg_new, pos_new_neg_new_vectors);
//	for(auto it = pos_new_neg_new_vectors.cbegin(); it != pos_new_neg_new_vectors.cend(); ++it){
//		const ZBDDNode* node = zddManager->createPath(**it);
//		delete *it;
//		pos_new_neg_new_node = zddManager->unionSubsumFree(pos_new_neg_new_node, node);
//	}

//	//for debugging only
//	std::cout << "product and filter\n";
//	ToString::printOutClauses(pos_new_neg_new_node);
	return pos_new_neg_new_node;
}

void doGRPerVariable_zbdd(int i, std::vector<std::vector<int>*>* vectors_pos_new, std::vector<std::vector<int>*>* vectors_neg_old,
		std::vector<std::vector<int>*>* vectors_neg_new, std::vector<std::vector<int>*>* vectors_pos_old,
		Logger* logger, unsigned length_threshold, ZBDDClauses* clauses_tmp){
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* pos_new = buildClauses(i, true, vectors_pos_new, zddManager);
	const ZBDDNode* neg_old = buildClauses(i, false, vectors_neg_old, zddManager);
	const ZBDDNode* neg_new = buildClauses(i, false, vectors_neg_new, zddManager);
	const ZBDDNode* pos_old = buildClauses(i, true, vectors_pos_old, zddManager);

	const ZBDDNode* pos_new_neg_new = productFilter(pos_new, neg_new, zddManager, length_threshold);
	const ZBDDNode* pos_new_neg_old = productFilter(pos_new, neg_old, zddManager, length_threshold);
	const ZBDDNode* neg_new_pos_old = productFilter(neg_new, pos_old, zddManager, length_threshold);

	const ZBDDNode* node_clauses = ZBDDManager::ZERO;
	node_clauses = zddManager->unionSubsumFree(node_clauses, pos_new_neg_new);
	node_clauses = zddManager->unionSubsumFree(node_clauses, pos_new_neg_old);
	node_clauses = zddManager->unionSubsumFree(node_clauses, neg_new_pos_old);

	clauses_tmp->add(node_clauses);

	zddManager->cleanNodes();
	delete zddManager;

	//clean vectors
	vectors_pos_new[i].clear();
	vectors_neg_old[i].clear();
	vectors_neg_new[i].clear();
	vectors_pos_old[i].clear();
}

std::vector<int>* doResolution(std::vector<int>* first, std::vector<int>* second, int key) {
	std::vector<int>* vector = new std::vector<int>;
	auto it_f = first->cbegin();
	auto it_s = second->cbegin();
	while(it_f != first->cend() && it_s != second->cend()){
		int l_f = *it_f;
		int l_s = *it_s;
		if(std::abs(l_f) == key){
			it_f++;
			continue;
		}
		if(std::abs(l_s) == key){
			it_s++;
			continue;
		}

		if(l_f == l_s){
			if(!vector->empty() && l_f == -vector->back()){
				return 0;
			}
			vector->push_back(l_f);
			it_f++;
			it_s++;
		}
		else if(sortLiterals(l_f, l_s)){
			if(!vector->empty() && l_f == -vector->back()){
				return 0;
			}
			vector->push_back(l_f);
			it_f++;
		}
		else{
			if(!vector->empty() && l_s == -vector->back()){
				return 0;
			}
			vector->push_back(l_s);
			it_s++;
		}
	}

	while(it_f != first->cend()){
		int l_f = *it_f;
		if(std::abs(l_f) == key){
			it_f++;
			continue;
		}
		if(!vector->empty() && l_f == -vector->back()){
			return 0;
		}
		vector->push_back(l_f);
		it_f++;
	}
	while(it_s != second->cend()){
		int l_s = *it_s;
		if(std::abs(l_s) == key){
			it_s++;
			continue;
		}
		if(!vector->empty() && l_s == -vector->back()){
			return 0;
		}
		vector->push_back(l_s);
		it_s++;
	}

	return vector;
}

void printVectorforDebugging(std::vector<int>* vector){
	std::cout << "[";
	for(auto it = vector->cbegin(); it != vector->cend(); ++it){
		std::cout << *it << ", ";
	}
	std::cout << "]\n";
}

void doGRPerPair(int i, std::vector<std::vector<int>*>* vectors_first, std::vector<std::vector<int>*>* vectors_second, ZBDDClauses* clauses_new,
		Logger* logger, unsigned length_threshold){
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* node_clauses = ZBDDManager::ZERO;

	for(auto it_first = vectors_first[i].cbegin(); it_first != vectors_first[i].cend(); ++it_first){
		std::vector<int>* clause_first = *it_first;
		for(auto it_second = vectors_second[i].cbegin(); it_second != vectors_second[i].cend(); ++it_second){
			std::vector<int>* clause_second = *it_second;
//			//for debugging
//			std::cout << i + 1 << std::endl;
//			printVectorforDebugging(clause_first);
//			printVectorforDebugging(clause_second);

			std::vector<int>* resultClause = doResolution(clause_first, clause_second, i + 1);

			if(resultClause){
//				//for debugging
//				printVectorforDebugging(resultClause);
//				std::cout << std::endl;
				if(resultClause->size() <= length_threshold){
					const ZBDDNode* path = zddManager->createPath(*resultClause);
					node_clauses = zddManager->unionSubsumFree(node_clauses, path);
				}
				delete resultClause;
			}
		}
	}

	clauses_new->add(node_clauses);

	zddManager->cleanNodes();
	delete zddManager;

}

void doGRPerVariable_vector(int i, std::vector<std::vector<int>*>* vectors_pos_new, std::vector<std::vector<int>*>* vectors_neg_old,
		std::vector<std::vector<int>*>* vectors_neg_new, std::vector<std::vector<int>*>* vectors_pos_old,
		Logger* logger, unsigned length_threshold, ZBDDClauses* clauses_tmp){
	doGRPerPair(i, vectors_pos_new, vectors_neg_old, clauses_tmp, logger, length_threshold);
	doGRPerPair(i, vectors_neg_new, vectors_pos_old, clauses_tmp, logger, length_threshold);
	doGRPerPair(i, vectors_neg_new, vectors_pos_new, clauses_tmp, logger, length_threshold);

	//clean vectors
	vectors_pos_new[i].clear();
	vectors_neg_new[i].clear();
	vectors_pos_old[i].clear();
	vectors_neg_old[i].clear();
}

void doGRPerVariable(int i, std::vector<std::vector<int>*>* vectors_pos_new, std::vector<std::vector<int>*>* vectors_neg_old,
		std::vector<std::vector<int>*>* vectors_neg_new, std::vector<std::vector<int>*>* vectors_pos_old,
		Logger* logger, unsigned length_threshold, ZBDDClauses* clauses_tmp){
	doGRPerVariable_zbdd(i, vectors_pos_new, vectors_neg_old, vectors_neg_new, vectors_pos_old, logger, length_threshold, clauses_tmp);
//	doGRPerVariable_vector(i, vectors_pos_new, vectors_neg_old, vectors_neg_new, vectors_pos_old, logger, length_threshold, clauses_tmp);
}

void addClauses_atomic(ZBDDClauses* clauses_new, ZBDDClauses* clauses_tmp){
	std::lock_guard<std::mutex> _(lock_add_clauses);
	clauses_new->add(clauses_tmp);
}

void doGRVariables(std::vector<int> & vector, std::vector<std::vector<int>*>* vectors_pos_new, std::vector<std::vector<int>*>* vectors_neg_new,
		std::vector<std::vector<int>*>* vectors_pos_old, std::vector<std::vector<int>*>* vectors_neg_old,
		ZBDDClauses* clauses_new, Logger* logger, unsigned length_threshold){
	ZBDDClauses* clauses_tmp = new ZBDDClauses();

	for(auto it = vector.cbegin(); it != vector.cend(); ++it){
		doGRPerVariable(*it, vectors_pos_new, vectors_neg_old, vectors_neg_new, vectors_pos_old, logger, length_threshold, clauses_tmp);
	}

	addClauses_atomic(clauses_new, clauses_tmp);

	//clean temporary clauses
	clauses_tmp->clean();
	delete clauses_tmp;
}

unsigned divideTasks(int numOfVariables, unsigned num_threads, std::vector<int>* & subtask_vectors){
	unsigned num_subs = num_threads * 2;
	subtask_vectors = new std::vector<int>[num_subs];
	for(int i = 0; i < numOfVariables; ++i){
		subtask_vectors[i % num_subs].push_back(i);
	}
	return num_subs;
}

void doGRPerIteration(std::vector<std::vector<int>*>* vectors_pos_new, std::vector<std::vector<int>*>* vectors_neg_new,
		std::vector<std::vector<int>*>* vectors_pos_old, std::vector<std::vector<int>*>* vectors_neg_old,
		ZBDDClauses* clauses_new, Logger* logger, unsigned length_threshold, int numOfVariables, unsigned num_threads){
	std::vector<int>* subtask_vectors;
	unsigned num_subs = divideTasks(numOfVariables, num_threads, subtask_vectors);

	//Create an asio::io_service and a thread_group (thread pool in essence)
	boost::asio::io_service ioService;
	boost::thread_group threadpool;

	//This will start the ioService processing loop. All tasks assigned with ioService.post() will start executing.
	boost::asio::io_service::work* working = new boost::asio::io_service::work(ioService);

	//This will add threads to the thread pool.
	for(unsigned i = 0; i < num_threads; ++i){
		threadpool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
	}

	//This will assign tasks to the thread pool.
	for(unsigned i = 0; i < num_subs; ++i){
		ioService.post(boost::bind(doGRVariables, subtask_vectors[i], vectors_pos_new, vectors_neg_new,
				vectors_pos_old, vectors_neg_old, clauses_new, logger, length_threshold));
	}

	//This will stop the ioService processing loop.
	//Any tasks you add behind this point will not execute.
	delete working;

	//Will wait till all the treads in the thread pool are finished with their assigned tasks and 'join' them.
	//Just assume the threads inside the threadpool will be destroyed by this method.
	threadpool.join_all();

	delete[] subtask_vectors;

//	//for debugging only
//	std::cout << "newly generated clauses: " << std::endl;
//	std::cout << *clauses_new << std::endl;
}

bool isUnSAT(std::vector<std::vector<int>*>& old_diff_vectors, std::vector<std::vector<int>*>& new_diff_vectors){
	std::unordered_set<int> set;
	for(auto it = old_diff_vectors.cbegin(); it != old_diff_vectors.cend(); ++it){
		if((*it)->size() == 1){
			int literal = (*it)->front();
			if(set.find(-literal) != set.end()){
				return true;
			}
			set.insert(literal);
		}
	}

	for(auto it = new_diff_vectors.cbegin(); it != new_diff_vectors.cend(); ++it){
		if((*it)->size() == 1){
			int literal = (*it)->front();
			if(set.find(-literal) != set.end()){
				return true;
			}
			set.insert(literal);
		}
	}

	return false;
}

void Preprocessor_GR::doGeneralResolution(){
	uint32 logLevel = 4;
	std::vector<std::vector<int>*> old_diff_vectors;
	std::vector<std::vector<int>*> new_diff_vectors;


	ZBDDClauses* clauses_new = new ZBDDClauses();
	ZBDDClauses* clauses_old = new ZBDDClauses();

	std::vector<std::vector<int>*>* vectors_pos_new = new std::vector<std::vector<int>*>[this->numOfVariables];
	std::vector<std::vector<int>*>* vectors_neg_new = new std::vector<std::vector<int>*>[this->numOfVariables];
	std::vector<std::vector<int>*>* vectors_pos_old = new std::vector<std::vector<int>*>[this->numOfVariables];
	std::vector<std::vector<int>*>* vectors_neg_old = new std::vector<std::vector<int>*>[this->numOfVariables];

	logger->Log("assign clauses initially\n", logLevel);
	assignClausesInit(vectors_pos_new, vectors_neg_new, clauses_old);
//	//for debugging only
//	ToString::printOutClauseVectors(vectors_pos_new, numOfVariables);
//	ToString::printOutClauseVectors(vectors_neg_new, numOfVariables);
//	std::cout << *clauses_old << std::endl;

	while(true){
		//do resolution
		Timer rm_gr("gr");
		logger->Log("do resolution\n", logLevel);
		doGRPerIteration(vectors_pos_new, vectors_neg_new, vectors_pos_old, vectors_neg_old, clauses_new, logger, length_threshold, numOfVariables, num_threads);
//		std::cout << rm_gr.result();
		logger->Log(rm_gr.result() + "\n", logLevel);

		Timer rm_other("other");
		//memory clean
		logger->Log("memory clean\n", logLevel);
		old_diff_vectors.erase(std::remove_if(old_diff_vectors.begin(), old_diff_vectors.end(), cleanVectorClauseGarbage), old_diff_vectors.end());
		old_diff_vectors.clear();
		new_diff_vectors.erase(std::remove_if(new_diff_vectors.begin(), new_diff_vectors.end(), cleanVectorClauseGarbage), new_diff_vectors.end());
		new_diff_vectors.clear();

		//subsumption elimination
		logger->Log("subsumption elimination\n", logLevel);
		ZBDDManager* zddManager_old = new ZBDDManager();
		const ZBDDNode* old_diff = zddManager_old->subsumedDiff(clauses_old->getRoot(), clauses_new->getRoot());
		ZBDDManager::getPaths(old_diff, old_diff_vectors);
		zddManager_old->cleanNodes();
		delete zddManager_old;
//		//for debugging
//		ToString::printOutInputs(old_diff_vectors);

		ZBDDManager* zddManager_new = new ZBDDManager();
		const ZBDDNode* new_diff = zddManager_new->subsumedDiff(clauses_new->getRoot(), clauses_old->getRoot());
		ZBDDManager::getPaths(new_diff, new_diff_vectors);
		zddManager_new->cleanNodes();
		delete zddManager_new;
//		//for debugging
//		ToString::printOutInputs(new_diff_vectors);

		//check if it's UnSAT
		if(isUnSAT(old_diff_vectors, new_diff_vectors)){
			throw UnSATPreprocessException();
		}

		//clean and reset zbdd clauses
		logger->Log("update and reset zbdd clauses\n", logLevel);
		clauses_old->add(clauses_new);
		clauses_new->cleanReset();


		//assign clauses accordingly and free clause_vector
		logger->Log("assign clauses accordingly\n", logLevel);
		assignClauses(old_diff_vectors, vectors_pos_old, vectors_neg_old);
//		//for debugging only
//		ToString::printOutClauseVectors(vectors_pos_old, numOfVariables);
//		ToString::printOutClauseVectors(vectors_neg_old, numOfVariables);
//		std::cout << *clauses_old << std::endl;
		assignClauses(new_diff_vectors, vectors_pos_new, vectors_neg_new);
//		//for debugging only
//		ToString::printOutClauseVectors(vectors_pos_new, numOfVariables);
//		ToString::printOutClauseVectors(vectors_neg_new, numOfVariables);
//		std::cout << *clauses_old << std::endl;

//		std::cout << rm_other.result() << std::endl;
		logger->Log(rm_other.result() + "\n", logLevel);

		//terminate condition
		logger->Log("terminate condition check\n\n", logLevel);
		if(new_diff_vectors.empty()){
			break;
		}
	}

	//memory clean
	old_diff_vectors.erase(std::remove_if(old_diff_vectors.begin(), old_diff_vectors.end(), cleanVectorClauseGarbage), old_diff_vectors.end());
//	old_diff_vectors.clear();
	new_diff_vectors.erase(std::remove_if(new_diff_vectors.begin(), new_diff_vectors.end(), cleanVectorClauseGarbage), new_diff_vectors.end());
//	new_diff_vectors.clear();

	delete[] vectors_pos_new;
	delete[] vectors_neg_new;
	delete[] vectors_pos_old;
	delete[] vectors_neg_old;

	assert(clauses_new->isEmpty());
	delete clauses_new;

	clauses = clauses_old;
//	//for debugging only
//	std::cout << *clauses << std::endl;
}

struct remove_short{
	unsigned length_thres;
public:
	remove_short(unsigned length_thres) : length_thres(length_thres){}
	bool operator()(const std::vector<int>* vector_literals) const {
		return vector_literals->size() <= length_thres + 1;
	}
};

void Preprocessor_GR::mergeInputs(){
	unsigned length_thres = length_threshold;
	auto riit = std::remove_if(inputs.begin(), inputs.end(), remove_short(length_thres));
	inputs.erase(riit, inputs.end());
//	//for debugging only
//	ToString::printOutInputs(inputs);

	//merge the inferred short clauses into inputs
	std::vector<std::vector<int>*> short_clauses_vector;
	ZBDDManager::getPaths(clauses->getRoot(), short_clauses_vector);
//	//for debugging only
//	ToString::printOutInputs(short_clauses_vector);

	for(auto it = short_clauses_vector.cbegin(); it != short_clauses_vector.cend(); ++it){
		inputs.push_back(*it);
	}
//	//for debugging only
//	ToString::printOutInputs(inputs);
}
