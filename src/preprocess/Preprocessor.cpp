/*
 * Preprocessor.cpp
 *
 *  Created on: Dec 19, 2016
 *      Author: icuzzq
 */

#include "Preprocessor.hpp"

//Preprocessor::Preprocessor(){
//
//}

//Preprocessor::Preprocessor(Logger* logger, const std::string& inputfileName, unsigned int num_threads): logger(logger), inputFile(inputfileName), num_threads(num_threads) {
//
//}
//
//Preprocessor::~Preprocessor(){
//	//deallocate inputs
////	clean();
//}

void Preprocessor::clean(){
	//deallocate inputs
//	for (auto vector_literals : inputs) {
//		delete vector_literals;
//	}

	for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
		delete *it;
	}

	delete [] staticOrdering;
}




/*check the cnf input whether its literals are valid (within the valid name range)
 * and at the same time, remove duplicates and tautological clauses  */
void Preprocessor::checkInputValidation(unsigned int& numOfVars){
    // read the file
    std::ifstream infile{inputFile};
    if(infile.fail()){
    	throw InputFileErrorException();
    }
    std::string line;
    while (std::getline(infile, line)) {
        // don't handle lines starting with c
        if (line[0] == 'c') {
            continue;
        }

        // split the string
        std::vector<std::string> splitStr;
        boost::split(splitStr, line, boost::is_any_of("\t "));

        // get the total number of variables
        if (line[0] == 'p') {
            numOfVariables = std::stoi(splitStr[splitStr.size() - 2]);
            numOfVars = numOfVariables;
            logger->Log2("\tnumber of variables: ").Log2(numOfVariables).Log2("\n\n");
            continue;
        }

        std::unordered_set<int> set_literals;
        std::vector<int>* vector_literals = new std::vector<int>;

        // loop through the splitStr
        for (unsigned int i = 0; i < splitStr.size() - 1; ++i) {
        	int literal = std::stoi(splitStr[i]);
//        	std::cout << literal << std::endl;

        	//check whether a literal is valid
        	unsigned int abs_literal = abs(literal);
        	if(abs_literal < 1 || abs_literal > numOfVariables){
        		throw InvalidInputException();
        	}

            //check tautology
            if(set_literals.find(-literal) != set_literals.end()){
            	logger->Log2("tautology!!!\n");
            	delete vector_literals;
            	vector_literals = 0;
            	break;
            }

        	//check duplicates
            if(set_literals.find(literal) != set_literals.end()){
            	logger->Log2("duplicated literal!!!\n");
            }
            else{
				set_literals.insert(literal);
				vector_literals->push_back(literal);
            }
        }

        if(vector_literals){
			inputs.push_back(vector_literals);
        }
    }

}

static int getScore(std::pair<int, int>& pair){
	if(pair.first * pair.second == 0){
		return INT_MIN;
	}
	return pair.first * pair.second - pair.first - pair.second;
}

void Preprocessor::getOrdering(){
	std::unordered_map<int, std::pair<int, int>> map_counts;

	for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
		std::vector<int>* vector_literals = *it;
		for(auto iit = vector_literals->cbegin(); iit != vector_literals->cend(); ++iit){
			int literal = *iit;
			int abs_literal = abs(literal);
			if(map_counts.find(abs_literal) != map_counts.end()){
				if(literal < 0){
					map_counts[abs_literal].first++;
				}
				else{
					map_counts[abs_literal].second++;
				}
			}
			else{
				if(literal < 0){
					map_counts[abs_literal] = std::make_pair(1, 0);
				}
				else{
					map_counts[abs_literal] = std::make_pair(0, 1);
				}
			}
		}
	}

	std::multimap<int, int> sorted_variables;
	uint32 level= 6;
	logger->Log("The counts info for each variable: \n", level);
	for(auto it = map_counts.cbegin(); it != map_counts.cend(); ++it){
		auto entry = *it;
		//for debugging
		logger->Log(entry.first, level).Log(": (", level).Log(entry.second.first, level).Log(",", level).Log(entry.second.second, level).Log(")\n", level);

		int score = getScore(entry.second);
		sorted_variables.insert(std::make_pair(score, entry.first));
	}
	logger->Log("\n", level);


    // loop through counts and populate static ordering
    staticOrdering = new int[numOfVariables];
    int index = 1;
    logger->Log("The sorted variables info: \n", level);
    for (auto iter = sorted_variables.rbegin(); iter != sorted_variables.rend(); ++iter) {
    	//for debugging
    	logger->Log(iter->first, level).Log(": ", level).Log(iter->second, level).Log("\n", level);

    	staticOrdering[iter->second - 1] = index++;
    }
    logger->Log("\n", level);

}


void Preprocessor::renameCNF(Bucket_flag** &buckets){
    // init the buckets
    buckets = new Bucket_flag*[numOfVariables];
    for (unsigned int i = 0; i < numOfVariables; ++i) {
    	ZBDDClauses* clauses = new ZBDDClauses();
        buckets[i] = new Bucket_flag{i+1, clauses};
    }

	for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
		std::vector<int>* vector_literals = *it;

		unsigned literalSize = vector_literals->size();
		Literal* literals = new Literal[literalSize];
		unsigned index = 0;
		for(auto iit = vector_literals->cbegin(); iit != vector_literals->cend(); ++iit){
			int literal = *iit;
			int abs_literal = abs(literal);

			int abs_new = staticOrdering[abs_literal - 1];
			int new_literal = (literal > 0) ? abs_new : -abs_new;
			literals[index++] = Literal{new_literal};
		}

//		//for debugging
//		for(unsigned i = 0; i < literalSize; ++i){
//			std::cout << literals[i] << ", ";
//		}
//		std::cout << std::endl;

		// sort the literals
		std::sort(literals, literals + literalSize);

//		//for debugging
//		for(unsigned i = 0; i < literalSize; ++i){
//			std::cout << literals[i] << ", ";
//		}
//		std::cout << std::endl << std::endl;

        // instantiate the clause
        Clause clause{literals, literalSize};

        // add the clause to the right bucket
        buckets[clause.getKey() - 1]->add(clause);
	}
}

void Preprocessor::renameCNF(Bucket_flag** &buckets, ZBDDManager* allManager, const ZBDDNode* & all){
    // init the buckets
    buckets = new Bucket_flag*[numOfVariables];
    for (unsigned int i = 0; i < numOfVariables; ++i) {
    	ZBDDClauses* clauses = new ZBDDClauses();
        buckets[i] = new Bucket_flag{i+1, clauses};
    }

	for(auto it = inputs.cbegin(); it != inputs.cend(); ++it){
		std::vector<int>* vector_literals = *it;

		unsigned literalSize = vector_literals->size();
		Literal* literals = new Literal[literalSize];
		unsigned index = 0;
		for(auto iit = vector_literals->cbegin(); iit != vector_literals->cend(); ++iit){
			int literal = *iit;
			int abs_literal = abs(literal);

			int abs_new = staticOrdering[abs_literal - 1];
			int new_literal = (literal > 0) ? abs_new : -abs_new;
			literals[index++] = Literal{new_literal};
		}

//		//for debugging
//		for(unsigned i = 0; i < literalSize; ++i){
//			std::cout << literals[i] << ", ";
//		}
//		std::cout << std::endl;

		// sort the literals
		std::sort(literals, literals + literalSize);

//		//for debugging
//		for(unsigned i = 0; i < literalSize; ++i){
//			std::cout << literals[i] << ", ";
//		}
//		std::cout << std::endl << std::endl;

        // instantiate the clause
        Clause clause{literals, literalSize};
//
//        // add the clause to the right bucket
//        buckets[clause.getKey() - 1]->add(clause);

		const ZBDDNode* node = allManager->createPath(clause);
		all = allManager->union_zbdd(all, node);
	}
}

