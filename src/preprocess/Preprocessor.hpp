/*
 * Preprocessor.hpp
 *
 *  Created on: Dec 19, 2016
 *      Author: icuzzq
 */

#ifndef PREPROCESS_PREPROCESSOR_HPP_
#define PREPROCESS_PREPROCESSOR_HPP_

#include "../common/BigSATCommon.hpp"
#include "../common/BigSATException.hpp"
#include "../commonimpl/Bucket_flag.hpp"
#include "../utility/Logger.hpp"
#include "../utility/ToString.hpp"

class Preprocessor {
public:
//	Preprocessor();
	Preprocessor(Logger* logger, const std::string& inputfileName, unsigned int num_threads) : logger(logger), inputFile(inputfileName), num_threads(num_threads){

	}

	~Preprocessor(){

	}

//	void process(int& numOfVars, Bucket_flag** &buckets, ZBDDManager* allManager, const ZBDDNode* all){
//		logger->Log2("check input validation...\n\n");
//		checkInputValidation(numOfVars);
//
//		logger->Log2("get variable ordering...\n\n");
//		getOrdering();
//
//		logger->Log2("rename CNF...\n\n");
//		renameCNF(buckets);
//	}

	void clean();


protected:
    /* Declaring Variables */
	Logger* logger;

	std::string inputFile;
	unsigned int num_threads;
	unsigned int numOfVariables;

	std::vector<std::vector<int>*> inputs;
    int* staticOrdering;


	// This method will check the cnf input whether its literals are valid (within the valid name range)
    // and at the same time, remove duplicates and tautological clauses
	void checkInputValidation(unsigned int& numOfVariables);

    // This method will compute the static orderings of the current input file
    // and return the mapping
    void getOrdering();

    // This method will preprocess the file with the static ordering to rename the input clauses
    // and return the buckets
    void renameCNF(Bucket_flag** &buckets);
    void renameCNF(Bucket_flag** &buckets, ZBDDManager* allManager, const ZBDDNode* & all);
};


#endif /* PREPROCESS_PREPROCESSOR_HPP_ */
