/*
 * Preprocessor_Sharp.hpp
 *
 *  Created on: Jul 19, 2017
 *      Author: icuzzq
 */

#ifndef PREPROCESS_PREPROCESSOR_CUDD_HPP_
#define PREPROCESS_PREPROCESSOR_CUDD_HPP_

#include "Preprocessor.hpp"
#include "../datastructure/AbstractBuckets.hpp"

class Preprocessor_lib : public Preprocessor {

public:

	Preprocessor_lib(Logger* logger, const std::string& inputfileName, unsigned int num_threads) : Preprocessor(logger, inputfileName, num_threads){}

	~Preprocessor_lib(){}

	void process(unsigned int& num_of_variables, AbstractBuckets* buckets){
		logger->Log2("check input validation...\n\n");
		checkInputValidation(num_of_variables);
		buckets->setVariableCount(num_of_variables);

		logger->Log2("get variable ordering...\n\n");
		getOrdering();

		logger->Log2("rename CNF...\n\n");
		renameCNF(buckets);
	}


private:

    // This method will preprocess the file with the static ordering to rename the input clauses
    // and return the buckets
    void renameCNF(AbstractBuckets* buckets);

};



#endif /* PREPROCESS_PREPROCESSOR_CUDD_HPP_ */
