/*
 * Preprocessor_GR.hpp
 *
 *  Created on: Dec 27, 2016
 *      Author: icuzzq
 */

#ifndef PREPROCESS_PREPROCESSOR_GR_HPP_
#define PREPROCESS_PREPROCESSOR_GR_HPP_

#include "Preprocessor.hpp"

class Preprocessor_GR : public Preprocessor {
public:
	Preprocessor_GR(Logger* logger, const std::string& inputfileName, unsigned int num_threads, unsigned int length_thres)
	: Preprocessor(logger, inputfileName, num_threads) {
		this->length_threshold = length_thres;
		this->clauses = new ZBDDClauses();
	}

	~Preprocessor_GR(){

	}

	void process(unsigned int& numOfVars, Bucket_flag** &buckets, ZBDDManager* allManager, const ZBDDNode* & all){
		logger->Log2("check input validation...\n\n");
		checkInputValidation(numOfVars);

		if(this->length_threshold > 0){
			logger->Log2("do general resolution...\n\n");
			doGeneralResolution();

			logger->Log2("merge inputs...\n\n");
			mergeInputs();
		}

		logger->Log2("get variable ordering...\n\n");
		getOrdering();

		logger->Log2("rename CNF...\n\n");
		renameCNF(buckets, allManager, all);
//		renameCNF(buckets);
	}

	void clean();

private:
	unsigned int length_threshold;

	ZBDDClauses * clauses;


	//do general resolution to infer short clauses
	void doGeneralResolution();

	//at the beginning, assign clauses accordingly
	void assignClausesInit(std::vector<std::vector<int>*>* vectors_pos_new, std::vector<std::vector<int>*>* vectors_neg_new, ZBDDClauses* clauses_old);

	//merge the inferred short clauses into the original inputs
	void mergeInputs();

};



#endif /* PREPROCESS_PREPROCESSOR_GR_HPP_ */
