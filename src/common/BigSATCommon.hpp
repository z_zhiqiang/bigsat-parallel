/*
 * BigSATCommon.hpp
 *
 *  Created on: Dec 18, 2016
 *      Author: icuzzq
 */

#ifndef COMMON_BIGSATCOMMON_HPP_
#define COMMON_BIGSATCOMMON_HPP_

#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <vector>
#include <ostream>
#include <cmath>

#include <cstdint>
#include <time.h>
#include <signal.h>

#include <ctime>
#include <sstream>
#include <iomanip>

#include <functional>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <stdlib.h>

#include <exception>

//#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <climits>
#include <memory>

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <mutex>

#include "../utility/ResourceManager.hpp"


typedef uint32_t uint32;
typedef int32_t int32;

#if !(__APPLE__ & __MACH__)
typedef uint64_t uint64;
typedef int64_t int64;
#else
typedef size_t uint64;
typedef size_t int64;
#endif

typedef uint16_t uint16;
typedef int16_t int16;
typedef int8_t int8;
typedef uint8_t uint8;





#endif /* COMMON_BIGSATCOMMON_HPP_ */
