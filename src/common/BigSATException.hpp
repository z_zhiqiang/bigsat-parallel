/*
 * BigSATException.hpp
 *
 *  Created on: Jan 2, 2017
 *      Author: icuzzq
 */

#ifndef COMMON_BIGSATEXCEPTION_HPP_
#define COMMON_BIGSATEXCEPTION_HPP_

#include "BigSATCommon.hpp"


struct InputFileErrorException : public std::exception {
	const char * what () const throw () {
		return "Invalid file--input error!!!";
	}
};


struct InvalidInputException : public std::exception {
   const char * what () const throw () {
	   return "Invalid literal--out of scope!!!";
   }
};


struct UnSATPreprocessException : public std::exception {
   const char * what () const throw () {
	   return "UnSAT proven during preprocessing!!!";
   }
};


struct UnSATException : public std::exception {
   const char * what () const throw () {
	   return "UnSAT proven!!!";
   }
};


struct SATException : public std::exception {
   const char * what () const throw () {
	   return "SAT proven!!!";
   }
};

#endif /* COMMON_BIGSATEXCEPTION_HPP_ */
