
//#include "ZBDDManager.hpp"
#include "../common/BigSATCommon.hpp"

class ZBDDNode{

	//toString
	friend std::ostream& operator<<(std::ostream& os, const ZBDDNode& node);

public:
	//constructor & destructor
	ZBDDNode(int v, const ZBDDNode* ln, const ZBDDNode* hn);
	virtual ~ZBDDNode();


//	bool operator==(const ZBDDNode& other) const{
//		return (this) == ((other));
////		if(this->isZERO() || this->isONE() || other.isZERO() || other.isONE())
////			return value == other.value;
////		return value == other.value && *low == *(other.low) && *high == *(other.high);
//	}

	//encode a literal into a variable value in ZBDDs
	inline static int encode(int literal) {
		return literal > 0 ? 2 * literal : 2 * abs(literal) - 1;
	}

	//decode a variable value in ZBDDs back to its original literal value
	inline static int decode(int var) {
		return var % 2 == 0 ? var / 2 : 0 - (var + 1) / 2;
	}


	/*getters*/
	inline int getValue() const {
		return value;
	}

	inline unsigned int getKey() const {
		return abs(decode(value));
	}

	inline const ZBDDNode* getLow() const {
		return low;
	}

	inline const ZBDDNode* getHigh() const {
		return high;
	}

	/*determine whether a node is 1-terminal node*/
	inline bool isONE() const {
		return value == -1;
	}

	/*determine whether a node is 0-terminal node*/
	inline bool isZERO() const {
		return value == 0;
	}

	inline bool isEmpty() const {
		return isONE() || isZERO();
	}

	/*recursive function: get the total number of paths under this node*/
	const long count() const;

	static void cleanNodes(const ZBDDNode* root);


private:
	int value;
	const ZBDDNode * low;
	const ZBDDNode * high;
};




