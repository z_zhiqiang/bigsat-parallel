/*
 * ZBDDClauses.hpp
 *
 *  Created on: Nov 26, 2016
 *      Author: icuzzq
 */

#ifndef ZBDDIMPL_ZBDDCLAUSES_HPP_
#define ZBDDIMPL_ZBDDCLAUSES_HPP_

#include "../common/BigSATCommon.hpp"
#include "ZBDDManager.hpp"

class ZBDDClauses {

	//toString
	friend std::ostream& operator<<(std::ostream& os, const ZBDDClauses& clauses);

public:
//	static ZBDDManager* zddManager;

	ZBDDClauses();
	ZBDDClauses(const ZBDDNode* root);
	~ZBDDClauses();

	const unsigned int getKey() const;
	unsigned getSize() const;

	inline bool isEmpty() const{
		return root->isEmpty();
	}

	void add(std::vector<int>& list);
	void add(const Clause& clause);
	void add(const ZBDDNode* node_clauses);
	void add(const ZBDDClauses* data);

	ZBDDClauses* doResolution();

	void reset();

	void clean();

	void cleanReset();

	inline const ZBDDNode* getRoot() const {
		return root;
	}

	inline void setRoot(const ZBDDNode* newRoot){
		root = newRoot;
	}

private:
	const ZBDDNode* root;

};




#endif /* ZBDDIMPL_ZBDDCLAUSES_HPP_ */
