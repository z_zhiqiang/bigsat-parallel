/*
 * ZBDDNode.cpp
 *
 *  Created on: Nov 15, 2016
 *      Author: icuzzq
 */
#include "ZBDDNode.hpp"

ZBDDNode::ZBDDNode(int val, const ZBDDNode * ln, const ZBDDNode * hn): value(val), low(ln), high(hn) {}

ZBDDNode::~ZBDDNode(){
//	std::cout << "ZBDDNode destructor called..." << std::endl;
}


const long ZBDDNode::count() const {
	if(this->isONE()){
		return 1;
	}

	if(this->isZERO()){
		return 0;
	}

	return this->low->count() + this->high->count();
}


std::ostream& operator<<(std::ostream& os, const ZBDDNode& node){
	//TODO
	os << ZBDDNode::decode(node.value);
	return os;
}

void collectCleanZBDDNodes(const ZBDDNode* root, std::unordered_set<const ZBDDNode*>* garbage_set){
	if(root && garbage_set->find(root) == garbage_set->end() && !root->isEmpty()){
		garbage_set->insert(root);
		collectCleanZBDDNodes(root->getHigh(), garbage_set);
		collectCleanZBDDNodes(root->getLow(), garbage_set);
		delete root;
	}
}

//void cleanClauses(const ZBDDNode* root){
//	std::unordered_set<const ZBDDNode*>* garbage_set = new std::unordered_set<const ZBDDNode*>;
//	collectCleanZBDDNodes(root, garbage_set);
//	delete garbage_set;
//}

void ZBDDNode::cleanNodes(const ZBDDNode* root){
	std::unordered_set<const ZBDDNode*>* garbage_set = new std::unordered_set<const ZBDDNode*>;
	collectCleanZBDDNodes(root, garbage_set);
	delete garbage_set;
}

