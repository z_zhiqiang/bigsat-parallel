/*
 * ZBDDManager.cpp
 *
 *  Created on: Nov 15, 2016
 *      Author: icuzzq
 */

#include "ZBDDManager.hpp"


const ZBDDNode* ZBDDManager::ZERO = new ZBDDNode(0, 0, 0);
const ZBDDNode* ZBDDManager::ONE = new ZBDDNode(-1, 0, 0);

ZBDDManager::ZBDDManager(){}

ZBDDManager::~ZBDDManager(){
//	std::cout << "ZBDDManager destructor called..." << std::endl;
	this->unique_set.clear();
	this->marked_set.clear();
}

void ZBDDManager::cleanNodes(){
	for(auto it = this->unique_set.begin(); it != this->unique_set.end(); ++it){
		delete (*it).getNode();
	}
}

//whether a clause is a tautology
static bool isTautology(int val, int another_val){
	return (val + 1) % 2 == 0 && another_val == val + 1;
}


//the basic operation to make a node in ZBDDs
const ZBDDNode* ZBDDManager::mk(int val, const ZBDDNode* low, const ZBDDNode* high){
	if(high->isZERO()){
		return rebuildZBDD(low);
	}

	if(isTautology(val, high->getValue())){
		return rebuildZBDD(low);
	}

	const ZBDDNode* newNode = new ZBDDNode(val, low, high);
	ZBDDNode_pointer tid(newNode);

	std::unordered_set<ZBDDNode_pointer>::const_iterator got = unique_set.find(tid);

	if(got == unique_set.end()){
		unique_set.insert(tid);
		return newNode;
	}
	else{
		delete newNode;
		return (*got).getNode();
	}

}

const ZBDDNode* ZBDDManager::rebuildZBDD(const ZBDDNode* root){
	if(root->isEmpty()){
		return root;
	}

	ZBDDNode_pointer tid(root);
	std::unordered_set<ZBDDNode_pointer>::const_iterator got = unique_set.find(tid);
	if(got != unique_set.end()){
		return (*got).getNode();
	}

	const ZBDDNode* low = rebuildZBDD(root->getLow());
	const ZBDDNode* high = rebuildZBDD(root->getHigh());
	return mk(root->getValue(), low, high);
}

//create a zbdd encoding a clause
const ZBDDNode* ZBDDManager::createPath(std::vector<int>& list){
	const ZBDDNode* last = ONE;
	for(int index = list.size() - 1; index >= 0; index--){
		int i = list[index];
		int j = ZBDDNode::encode(i);
		last = mk(j, ZERO, last);
	}
	return last;
}

const ZBDDNode* ZBDDManager::createPath(std::vector<int>& list, int excluded_literal){
	const ZBDDNode* last = ONE;
	for(int index = list.size() - 1; index >= 0; index--){
		int i = list[index];
		if(i != excluded_literal){
			int j = ZBDDNode::encode(i);
			last = mk(j, ZERO, last);
		}
	}
	return last;
}

const ZBDDNode* ZBDDManager::createPath(const Clause& clause){
	const ZBDDNode* last = ONE;
	for(int index = clause.getSize() - 1; index >= 0; index--){
		int i = clause.getLiterals()[index].getLiteral();
		int j = ZBDDNode::encode(i);
		last = mk(j, ZERO, last);
	}
	return last;
}

const ZBDDNode* ZBDDManager::createPath(const Clause& clause, int excluded_literal){
	const ZBDDNode* last = ONE;
	for(int index = clause.getSize() - 1; index >= 0; index--){
		int i = clause.getLiterals()[index].getLiteral();
		if(i != excluded_literal){
			int j = ZBDDNode::encode(i);
			last = mk(j, ZERO, last);
		}
	}
	return last;
}

//the basic operation to make a node in ZBDDs
const ZBDDNode* mk_path(int val, const ZBDDNode* low, const ZBDDNode* high){
	if(high->isZERO()){
		return low;
	}

	if(isTautology(val, high->getValue())){
		return low;
	}

	ZBDDNode* newNode = new ZBDDNode(val, low, high);
	return newNode;
}

//create a zbdd encoding a clause
const ZBDDNode* ZBDDManager::createPath_static(std::vector<int>& list){
	const ZBDDNode* last = ONE;
	for(int index = list.size() - 1; index >= 0; index--){
		int i = list[index];
		int j = ZBDDNode::encode(i);
		last = mk_path(j, ZERO, last);
	}
	return last;
}

const ZBDDNode* ZBDDManager::createPath_static(const Clause& clause){
	const ZBDDNode* last = ONE;
	for(int index = clause.getSize() - 1; index >= 0; index--){
		int i = clause.getLiterals()[index].getLiteral();
		int j = ZBDDNode::encode(i);
		last = mk_path(j, ZERO, last);
	}
	return last;
}


const ZBDDNode* follow_low(const ZBDDNode* zdd){
	while(!zdd->isZERO() && !zdd->isONE()){
		zdd = zdd->getLow();
	}
	return zdd;
}

////regular intersection of two zbdds
//const ZBDDNode* ZBDDManager::intersect_zbdd(const ZBDDNode* p, const ZBDDNode* q){
//	if(p->isZERO() || q->isZERO()){
//		return ZERO;
//	}
//	if(p == q){
//		return (p);
//	}
//	if(p->isONE()){
//		return follow_low(q);
//	}
//	if(q->isONE()){
//		return follow_low(p);
//	}
//
//	if(p->getValue() < q->getValue()){
//		return intersect_zbdd(p->getLow(), q);
//	}
//	else if(p->getValue() > q->getValue()){
//		return intersect_zbdd(p, q->getLow());
//	}
//	else{
//		return mk(p->getValue(), intersect_zbdd(p->getLow(), q->getLow()), intersect_zbdd(p->getHigh(), q->getHigh()));
//	}
//}


//const ZBDDNode* ZBDDManager::insert_base(const ZBDDNode* zdd){
//	if(zdd->isONE() || zdd->isZERO()){
//		return ONE;
//	}
//	const ZBDDNode* node = insert_base(zdd->getLow());
//	return (zdd->getLow() == node) ? (zdd) : mk(zdd->getValue(), node, zdd->getHigh());
//}

//regular union of two zbdds
const ZBDDNode* ZBDDManager::union_zbdd(const ZBDDNode* p, const ZBDDNode* q){
	if(p->isZERO()){
		return q;
	}
	if(q->isZERO() || p == q){
		return p;
	}
	if(p->isONE() || q->isONE()){
		return ONE;
	}

	if(p->getValue() < q->getValue()){
		return union_zbdd(q, p);
	}
	else if(p->getValue() > q->getValue()){
		return mk(q->getValue(), union_zbdd(p, q->getLow()), q->getHigh());
	}
	else{
		return mk(p->getValue(), union_zbdd(p->getLow(), q->getLow()), union_zbdd(p->getHigh(), q->getHigh()));
	}
}


bool emptyIn(const ZBDDNode* zdd){
	return follow_low(zdd)->isONE();
}

const ZBDDNode* ZBDDManager::subsumedDiff_rec(const ZBDDNode* f, const ZBDDNode* c){
	if(f->isZERO() || c->isONE() || f == c){
		return ZERO;
	}
	if(f->isONE() || c->isZERO()){
		return (f);
	}

	int fvar = f->getValue(), cvar = c->getValue();
	if(fvar > cvar){
		return subsumedDiff_rec(f, c->getLow());
	}
	else if(fvar < cvar){
		return mk(fvar, subsumedDiff_rec(f->getLow(), c), subsumedDiff_rec(f->getHigh(), c));
	}
	else{
		const ZBDDNode* tmp;
		if(emptyIn(c->getHigh())){
			tmp = ZERO;
		}
		else{
			tmp = subsumedDiff_rec(subsumedDiff_rec(f->getHigh(), c->getHigh()), c->getLow());
		}
		return mk(fvar, subsumedDiff_rec(f->getLow(), c->getLow()), tmp);
	}
}

//subsumed difference, i.e., Definition 2 in Laurent Simon's paper
const ZBDDNode* ZBDDManager::subsumedDiff(const ZBDDNode* p, const ZBDDNode* q){
	if(emptyIn(q)){
		return ZERO;
	}
	return subsumedDiff_rec(p, q);
}


//subsumption elimination, Definition 3 in Laurent Simon's paper
const ZBDDNode* ZBDDManager::noSubsumption(const ZBDDNode* zdd){
	if(zdd->isONE()){
		return ONE;
	}
	if(zdd->isZERO()){
		return ZERO;
	}

	if(zdd->getLow() == zdd->getHigh()){
		return noSubsumption(zdd->getLow());
	}
	else{
		const ZBDDNode* tmplow = noSubsumption(zdd->getLow());
		const ZBDDNode* tmphigh = subsumedDiff(noSubsumption(zdd->getHigh()), tmplow);
		return mk(zdd->getValue(), tmplow, tmphigh);
	}
}


//subsumption-free union, Definition 4 in Laurent Simon's paper
const ZBDDNode* ZBDDManager::unionSubsumFree(const ZBDDNode* p, const ZBDDNode* q){
	if(p->isZERO()){
		return noSubsumption(q);
	}
	if(q->isZERO() || p == q){
		return noSubsumption(p);
	}
	if(p->isONE() || q->isONE()){
		return ONE;
	}

	if(p->getValue() < q->getValue()){
		return unionSubsumFree(q, p);
	}
	else if(p->getValue() > q->getValue()){
		const ZBDDNode* tmplow = unionSubsumFree(q->getLow(), p);
		const ZBDDNode* tmphigh = subsumedDiff(noSubsumption(q->getHigh()), tmplow);
		return mk(q->getValue(), tmplow, tmphigh);
	}
	else{
		const ZBDDNode* tmplow = unionSubsumFree(p->getLow(), q->getLow());
		const ZBDDNode* tmphigh = subsumedDiff(unionSubsumFree(p->getHigh(), q->getHigh()), tmplow);
		return mk(p->getValue(), tmplow, tmphigh);
	}
}


static bool hasConsecutiveVar(const ZBDDNode* p, const ZBDDNode* q){
	return q->getValue() == p->getValue() + 1;
}

static bool hasEqualVar(const ZBDDNode* p, const ZBDDNode* q){
	return p->getValue() == q->getValue();
}

bool ZBDDManager::isConsecutive(const ZBDDNode* zdd){
	return zdd->getLow()->getValue() == zdd->getValue() + 1;
}

bool ZBDDManager::isNegative(const ZBDDNode* zdd){
	return (zdd->getValue() + 1) % 2 == 0;
}

//subsumption- and tautology-free product, i.e., product of two ZBDDs while removing all the subsumed and tautological clauses
const ZBDDNode* ZBDDManager::productSTFree(const ZBDDNode* p, const ZBDDNode* q){
	if(p->isZERO() || q->isZERO()){
		return ZERO;
	}
	if(p->isONE()){
		return noSubsumption(q);
	}
	if(q->isONE()){
		return noSubsumption(p);
	}

	if(p->getValue() > q->getValue()){
		return productSTFree(q, p);
	}

	if(isNegative(p)){
		if(isConsecutive(p)){//R1, R2 and R3 of Definition 5 in Laurent Simon's paper
			const ZBDDNode* a1 = p->getHigh();
			const ZBDDNode* a2 = p->getLow()->getHigh();
			const ZBDDNode* a3 = p->getLow()->getLow();

			if(hasEqualVar(p, q)){//R1 and R2
				if(isConsecutive(q)){//R1: (-x, (x, A3, A2), A1) * (-x, (x, B3, B2), B1)
					const ZBDDNode* b1 = q->getHigh();
					const ZBDDNode* b2 = q->getLow()->getHigh();
					const ZBDDNode* b3 = q->getLow()->getLow();

					const ZBDDNode* tmpa3b3 = productSTFree(a3, b3);
					const ZBDDNode* tmpa1b1 = productSTFree(a1, b1);
					const ZBDDNode* tmpa1b3 = productSTFree(a1, b3);
					const ZBDDNode* tmpb1a3 = productSTFree(b1, a3);
					const ZBDDNode* tmpr1 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa1b3), tmpb1a3), tmpa3b3);

					const ZBDDNode* tmpa2b2 = productSTFree(a2, b2);
					const ZBDDNode* tmpa2b3 = productSTFree(a2, b3);
					const ZBDDNode* tmpb2a3 = productSTFree(b2, a3);
					const ZBDDNode* tmpr2 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa2b2, tmpa2b3), tmpb2a3), tmpa3b3);

					return mk(p->getValue(), mk(p->getLow()->getValue(), tmpa3b3, tmpr2), tmpr1);

				}
				else{//R2: (-x, (x, A3, A2), A1) * (-x, B2, B1) s.t. getVar(B2) != x
					const ZBDDNode* b1 = q->getHigh();
					const ZBDDNode* b2 = q->getLow();

					const ZBDDNode* tmpa3b2 = productSTFree(a3, b2);
					const ZBDDNode* tmpa1b1 = productSTFree(a1, b1);
					const ZBDDNode* tmpa1b2 = productSTFree(a1, b2);
					const ZBDDNode* tmpb1a3 = productSTFree(b1, a3);
					const ZBDDNode* tmpr1 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa1b2), tmpb1a3), tmpa3b2);

					const ZBDDNode* tmpa2b2 = productSTFree(a2, b2);
					const ZBDDNode* tmpr2 = subsumedDiff(tmpa2b2, tmpa3b2);

					return mk(p->getValue(), mk(p->getLow()->getValue(), tmpa3b2, tmpr2), tmpr1);
				}
			}
			else if(hasConsecutiveVar(p, q)){//R3: (-x, (x, A3, A2), A1) * (x, B2, B1)
				const ZBDDNode* b1 = q->getHigh();
				const ZBDDNode* b2 = q->getLow();

				const ZBDDNode* tmpa3b2 = productSTFree(a3, b2);
				const ZBDDNode* tmpa1b2 = productSTFree(a1, b2);
				const ZBDDNode* tmpr1 = subsumedDiff(tmpa1b2, tmpa3b2);

				const ZBDDNode* tmpa2b1 = productSTFree(a2, b1);
				const ZBDDNode* tmpa2b2 = productSTFree(a2, b2);
				const ZBDDNode* tmpb1a3 = productSTFree(b1, a3);
				const ZBDDNode* tmpr2 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa2b1, tmpa2b2), tmpb1a3), tmpa3b2);

				return mk(p->getValue(), mk(p->getLow()->getValue(), tmpa3b2, tmpr2), tmpr1);
			}
		}
		else if(hasEqualVar(p, q) && isConsecutive(q)){//symmetry of R2
			return productSTFree(q,p);
		}
		else if(hasConsecutiveVar(p, q)){//R4: (-x, A2, A1) * (x, B2, B1) s.t. getVar(A2) != x
			const ZBDDNode* a1 = p->getHigh();
			const ZBDDNode* a2 = p->getLow();
			const ZBDDNode* b1 = q->getHigh();
			const ZBDDNode* b2 = q->getLow();

			const ZBDDNode* tmpa2b2 = productSTFree(a2, b2);
			const ZBDDNode* tmpa1b2 = productSTFree(a1, b2);
			const ZBDDNode* tmpr1 = subsumedDiff(tmpa1b2, tmpa2b2);

			const ZBDDNode* tmpa2b1 = productSTFree(a2, b1);
			const ZBDDNode* tmpr2 = subsumedDiff(tmpa2b1, tmpa2b2);

			return mk(p->getValue(), mk(q->getValue(), tmpa2b2, tmpr2), tmpr1);
		}
	}

	if(hasEqualVar(q, p)){//R5
		const ZBDDNode* a1 = p->getHigh();
		const ZBDDNode* a2 = p->getLow();
		const ZBDDNode* b1 = q->getHigh();
		const ZBDDNode* b2 = q->getLow();

		const ZBDDNode* tmpa2b2 = productSTFree(a2, b2);
		const ZBDDNode* tmpa1b1 = productSTFree(a1, b1);
		const ZBDDNode* tmpa2b1 = productSTFree(a2, b1);
		const ZBDDNode* tmpa1b2 = productSTFree(a1, b2);
		const ZBDDNode* tmprh = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa2b1), tmpa1b2), tmpa2b2);

		return mk(p->getValue(), tmpa2b2, tmprh);
	}
	else{//R6 and R7 which are symmetry
		const ZBDDNode* tmpa2q = productSTFree(p->getLow(), q);
		const ZBDDNode* tmpa1q = productSTFree(p->getHigh(), q);
		const ZBDDNode* tmprh = subsumedDiff(tmpa1q, tmpa2q);

		return mk(p->getValue(), tmpa2q, tmprh);
	}

}

//split zdd into two disjoint zdds, such that the first generated zdd with cut_count size
std::pair<const ZBDDNode*, const ZBDDNode*> ZBDDManager::splitZDD(const ZBDDNode* zdd, long cut_count){
	assert(cut_count < zdd->count());

	if(zdd->count() <= 2){
		return std::make_pair(ZERO, zdd);
	}

	const ZBDDNode* cut_zdd = extractSubset_rec(zdd, cut_count, 0, ONE, ZERO);
	const ZBDDNode* remain_zdd = subsumedDiff(zdd, cut_zdd);
	return std::make_pair(cut_zdd, remain_zdd);
}

const ZBDDNode* ZBDDManager::extractSubset_rec(const ZBDDNode* zdd, long count, const ZBDDNode* parent, const ZBDDNode* prefix, const ZBDDNode* result){
	if(count < zdd->count()){
		return extractSubset_rec(zdd->getLow(), count, zdd, prefix, result);
	}
	else if(count > zdd->count()){
		count -= zdd->count();
		result = unionSubsumFree(result, productSTFree(zdd, prefix));
		return extractSubset_rec(parent->getHigh(), count, parent, productSTFree(prefix, mk(parent->getValue(), ZERO, ONE)), result);
	}
	else{
		return unionSubsumFree(result, productSTFree(zdd, prefix));
	}
}


//mark a node
void ZBDDManager::markNode(const ZBDDNode* zdd){
	this->marked_set.insert(zdd);
}

//test if a node is already marked
bool ZBDDManager::isMarked(const ZBDDNode* zdd){
	return this->marked_set.find(zdd) != this->marked_set.end();
}

//clear marked set
void ZBDDManager::clearMarkedSet(){
//	for(auto& entry: this->marked_set){
//		delete entry;
//	}
	this->marked_set.clear();
}

void ZBDDManager::printZDD_rec(const ZBDDNode* zdd){
	if(zdd->isZERO() || zdd->isONE()){
		return;
	}
	if(isMarked(zdd)){
		return;
	}
	std::cout << zdd << ".\t(" << ZBDDNode::decode(zdd->getValue()) << "):\t"
			<< zdd->getLow() << ",\t" << zdd->getHigh() << std::endl;
	markNode(zdd);
	printZDD_rec(zdd->getLow());
	if(zdd->getLow() != zdd->getHigh()){
		printZDD_rec(zdd->getHigh());
	}
}

//print zbdd
void ZBDDManager::printZDD(const ZBDDNode* zdd){
	if(zdd->isZERO()){
		std::cout << ". 0" << std::endl;
	}
	else if(zdd->isONE()){
		std::cout << ". 1" << std::endl;
	}
	else{
		printZDD_rec(zdd);
		clearMarkedSet();
	}
	std::cout << std::endl;
}


void ZBDDManager::getPaths_rec(const ZBDDNode* zdd, std::vector<std::vector<int>*>& list, std::vector<int>* innerlist){
	if(zdd->isZERO()){
		delete innerlist;
		return;
	}
	if(zdd->isONE()){
		list.push_back(innerlist);
		return;
	}

	std::vector<int>* newVl = new std::vector<int>();
	std::vector<int>* newVh = new std::vector<int>();
	for(auto it = innerlist->begin(); it != innerlist->end(); ++it){
		newVl->push_back(*it);
		newVh->push_back(*it);
	}
	delete innerlist;

	getPaths_rec(zdd->getLow(), list, newVl);
	newVh->push_back(ZBDDNode::decode(zdd->getValue()));
	getPaths_rec(zdd->getHigh(), list, newVh);
}

//get clauses encoded by ZBDDs
void ZBDDManager::getPaths(const ZBDDNode* zdd, std::vector<std::vector<int>*>& list){
	if(zdd->isZERO()){
		return;
	}
	else if(zdd->isONE()){
		std::vector<int>* inlist = new std::vector<int>;
		list.push_back(inlist);
		return;
	}
	else{
		std::vector<int>* innerlist = new std::vector<int>;
		getPaths_rec(zdd, list, innerlist);
		return;
	}
}

void ZBDDManager::getPaths_lengthlimit_rec(unsigned limit_length, const ZBDDNode* zdd, std::vector<std::vector<int>*>& list, std::vector<int>* innerlist){
	if(zdd->isZERO()){
		delete innerlist;
		return;
	}
	if(zdd->isONE()){
		list.push_back(innerlist);
		return;
	}

//	if(innerlist->size() > limit_length){
//		delete innerlist;
//		return;
//	}

	std::vector<int>* newVl = new std::vector<int>();
	std::vector<int>* newVh = new std::vector<int>();
	for(auto it = innerlist->begin(); it != innerlist->end(); ++it){
		newVl->push_back(*it);
		newVh->push_back(*it);
	}
	delete innerlist;

	getPaths_lengthlimit_rec(limit_length, zdd->getLow(), list, newVl);

	if(newVh->size() == limit_length){
		delete newVh;
		return;
	}

	newVh->push_back(ZBDDNode::decode(zdd->getValue()));
	getPaths_lengthlimit_rec(limit_length, zdd->getHigh(), list, newVh);
}

//get clauses encoded by ZBDDs
void ZBDDManager::getPaths_lengthlimit(unsigned limit_length, const ZBDDNode* zdd, std::vector<std::vector<int>*>& list){
	if(zdd->isZERO()){
		return;
	}
	else if(zdd->isONE()){
		std::vector<int>* inlist = new std::vector<int>;
		list.push_back(inlist);
		return;
	}
	else{
		std::vector<int>* innerlist = new std::vector<int>;
		getPaths_lengthlimit_rec(limit_length, zdd, list, innerlist);
		return;
	}
}

const ZBDDNode* ZBDDManager::createPaths_lengthlimit_rec(bool & flag_cut, unsigned limit_ub, unsigned limit_lb, const ZBDDNode* zdd, unsigned length){
	if(zdd->isZERO()){
		return ZERO;
	}
	if(zdd->isONE()){
		if(length < limit_lb){
			flag_cut = true;
			return ZERO;
		}
		return ONE;
	}

	const ZBDDNode* nlow = createPaths_lengthlimit_rec(flag_cut, limit_ub, limit_lb, zdd->getLow(), length);

	if(length == limit_ub){
		flag_cut = true;
		return nlow;
	}

	const ZBDDNode* nhigh = createPaths_lengthlimit_rec(flag_cut, limit_ub, limit_lb, zdd->getHigh(), length + 1);
	return mk(zdd->getValue(), nlow, nhigh);
}

//get clauses encoded by ZBDDs
const ZBDDNode* ZBDDManager::createPaths_lengthlimit(bool & flag_cut, unsigned limit_ub, unsigned limit_lb, const ZBDDNode* zdd){
//	if(zdd->isZERO()){
//		return ZERO;
//	}
//	else if(zdd->isONE()){
//		return ONE;
//	}
//	else{
//	}
	return createPaths_lengthlimit_rec(flag_cut, limit_ub, limit_lb, zdd, 0);
}
