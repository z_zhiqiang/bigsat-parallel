/*
 * ZBDDClauses.cpp
 *
 *  Created on: Nov 27, 2016
 *      Author: icuzzq
 */

#include "ZBDDClauses.hpp"

//ZBDDManager* ZBDDClauses::zddManager = new ZBDDManager();

ZBDDClauses::ZBDDClauses(){
	root = ZBDDManager::ZERO;
}

ZBDDClauses::ZBDDClauses(const ZBDDNode* root): root(root) {}

ZBDDClauses::~ZBDDClauses(){
//	std::cout << "ZBDDClauses destructor called..." << std::endl;
}


std::ostream& operator<<(std::ostream& os, const ZBDDClauses& clauses){
	if(clauses.getRoot()->isONE()){
		os << "[[]]" << std::endl;
	}
	else if(clauses.getRoot()->isZERO()){
		os << "[]" << std::endl;
	}

	std::vector<std::vector<int>*> paths;
	ZBDDManager::getPaths(clauses.root, paths);
	for(auto it = paths.cbegin(); it != paths.cend(); ++it){
		//print out a path
		for(auto init = (*it)->cbegin(); init != (*it)->cend(); ++init){
			os << *init << ", ";
		}
		os << std::endl;

		//clear
		delete *it;
	}
	os << std::endl;
	return os;
}

const unsigned int ZBDDClauses::getKey() const{
	return root->getKey();
}

unsigned ZBDDClauses::getSize() const{
	return root->count();
}

//void collectCleanZBDDNodes(const ZBDDNode* root, std::unordered_set<const ZBDDNode*>* garbage_set){
//	if(root && garbage_set->find(root) == garbage_set->end() && !root->isEmpty()){
//		garbage_set->insert(root);
//		collectCleanZBDDNodes(root->getHigh(), garbage_set);
//		collectCleanZBDDNodes(root->getLow(), garbage_set);
//		delete root;
//	}
//}
//
//void cleanClauses(const ZBDDNode* root){
//	std::unordered_set<const ZBDDNode*>* garbage_set = new std::unordered_set<const ZBDDNode*>;
//	collectCleanZBDDNodes(root, garbage_set);
//	delete garbage_set;
//}

void ZBDDClauses::add(std::vector<int>& list){
	const ZBDDNode* node = ZBDDManager::createPath_static(list);
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* tmp = zddManager->unionSubsumFree(root, node);
	delete zddManager;
	ZBDDNode::cleanNodes(node);
	this->clean();
	root = tmp;
}

void ZBDDClauses::add(const Clause& clause){
	const ZBDDNode* node = ZBDDManager::createPath_static(clause);
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* tmp = zddManager->unionSubsumFree(root, node);
	delete zddManager;
	ZBDDNode::cleanNodes(node);
	this->clean();
	root = tmp;
}

void ZBDDClauses::add(const ZBDDNode* node_clauses){
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* tmp = zddManager->unionSubsumFree(root, node_clauses);
	delete zddManager;
	this->clean();
	root = tmp;
}

void ZBDDClauses::add(const ZBDDClauses* data){
	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* tmp = zddManager->unionSubsumFree(root, data->getRoot());
	delete zddManager;
	this->clean();
	root = tmp;
}

ZBDDClauses* ZBDDClauses::doResolution(){
	assert(this->root->getKey() == this->root->getLow()->getKey() && this->root->getLow()->getLow()->isZERO());

	ZBDDManager* zddManager = new ZBDDManager();
	const ZBDDNode* node = zddManager->productSTFree(this->root->getHigh(), this->root->getLow()->getHigh());
	delete zddManager;
	return new ZBDDClauses(node);
}

void ZBDDClauses::reset(){

}

void ZBDDClauses::clean(){
	ZBDDNode::cleanNodes(root);
}

void ZBDDClauses::cleanReset(){
	clean();
	root = ZBDDManager::ZERO;
}



