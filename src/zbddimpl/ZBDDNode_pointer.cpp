/*
 * TripleID.cpp
 *
 *  Created on: Nov 17, 2016
 *      Author: icuzzq
 */
//#include <ostream>

#include "ZBDDNode_pointer.hpp"


/*
 * for class ZBDDNode_pointer
 */

ZBDDNode_pointer::ZBDDNode_pointer(const ZBDDNode* node) : node(node){}

ZBDDNode_pointer::~ZBDDNode_pointer(){
//	std::cout << "ZBDDNode_pointer destructor called..." << std::endl;
}

std::ostream& operator<<(std::ostream& os, const ZBDDNode_pointer& triple){
	//TODO
	os << triple.getValue() << ":" << triple.getLowID() << "|" << triple.getHighID();
	return os;
}

