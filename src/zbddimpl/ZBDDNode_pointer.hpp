/*
 * TripleID.hpp
 *
 *  Created on: Nov 17, 2016
 *      Author: icuzzq
 */

#ifndef TRIPLEID_HPP_
#define TRIPLEID_HPP_

#include "../common/BigSATCommon.hpp"
#include "ZBDDNode.hpp"


class ZBDDNode_pointer{

	friend std::ostream& operator<<(std::ostream& os, const ZBDDNode_pointer& triple);

public:
	ZBDDNode_pointer(const ZBDDNode* node);
	virtual ~ZBDDNode_pointer();

	//operator for map
	bool operator==(const ZBDDNode_pointer& other) const {
		return node->getValue() == other.node->getValue() && node->getLow() == other.node->getLow() && node->getHigh() == other.node->getHigh();
	}

	inline int getValue() const {
		return node->getValue();
	}

	inline std::uintptr_t getLowID() const {
		return (std::uintptr_t)(node->getLow());
	}

	inline std::uintptr_t getHighID() const {
		return (std::uintptr_t)(node->getHigh());
	}

	inline const ZBDDNode* getNode() const {
		return node;
	}


private:
	const ZBDDNode* node;
};


namespace std {
	template<>
	struct hash<ZBDDNode_pointer> {
		std::size_t operator()(const ZBDDNode_pointer& triple_key) const {
			//simple hash
//			return std::hash<int>()(triple_key.getValue())
//				+ std::hash<uint32>()(triple_key.getLowID())
//				+ std::hash<uint32>()(triple_key.getHighID())
//				;
			return std::hash<int>()(triple_key.getValue() ^ ((int (triple_key.getLowID()) << 8) | (int (triple_key.getHighID()))));
		}
	};
}


#endif /* TRIPLEID_HPP_ */
