/*
 * ZBDDManager.hpp
 *
 *  Created on: Nov 15, 2016
 *      Author: icuzzq
 */

#ifndef SRC_ZBDDIMPL_ZBDDMANAGER_HPP_
#define SRC_ZBDDIMPL_ZBDDMANAGER_HPP_


#include "../commonimpl/Clause.hpp"
#include "../common/BigSATCommon.hpp"
//#include "ZBDDNode.hpp"
#include "ZBDDNode_pointer.hpp"


class ZBDDManager{

public:
	const static ZBDDNode* ZERO;
	const static ZBDDNode* ONE;

	ZBDDManager();
	~ZBDDManager();


	//the basic operation to make a node in ZBDDs
	const ZBDDNode* mk(int val, const ZBDDNode* low, const ZBDDNode* high);

	//rebuild zbdd
	const ZBDDNode* rebuildZBDD(const ZBDDNode* root);

	//create a zbdd encoding a clause
	const ZBDDNode* createPath(std::vector<int>& list);

	const ZBDDNode* createPath(std::vector<int>& list, int excluded_literal);

	const ZBDDNode* createPath(const Clause& clause);

	const ZBDDNode* createPath(const Clause& clause, int excluded_literal);

	//create a zbdd encoding a clause
	static const ZBDDNode* createPath_static(std::vector<int>& list);

	static const ZBDDNode* createPath_static(const Clause& clause);

//	//intersect
//	const ZBDDNode* intersect_zbdd(const ZBDDNode* p, const ZBDDNode* q);

	//union
	const ZBDDNode* union_zbdd(const ZBDDNode* p, const ZBDDNode* q);

	//subsumed difference
	const ZBDDNode* subsumedDiff(const ZBDDNode* p, const ZBDDNode* q);

	//subsumption elimination
	const ZBDDNode* noSubsumption(const ZBDDNode* zdd);

	//subsumption-free union
	const ZBDDNode* unionSubsumFree(const ZBDDNode* p, const ZBDDNode* q);

	//subsumption- and tautology-free product
	const ZBDDNode* productSTFree(const ZBDDNode* p, const ZBDDNode* q);

	//split zdd into two disjoint zdds, such that the first generated zdd with cut_count size
	std::pair<const ZBDDNode*, const ZBDDNode*> splitZDD(const ZBDDNode* zdd, long cut_count);


	//for printing
	void printZDD(const ZBDDNode* zdd);

	//clean
	void cleanNodes();

	//get paths encoded by zdd
	static void getPaths(const ZBDDNode* zdd, std::vector<std::vector<int>*>& paths);
	static void getPaths_lengthlimit(unsigned limit_length, const ZBDDNode* zdd, std::vector<std::vector<int>*>& paths);
	const ZBDDNode* createPaths_lengthlimit(bool & flag, unsigned limit_ub, unsigned limit_lb, const ZBDDNode* zdd);

	static bool isNegative(const ZBDDNode* zdd);
	static bool isConsecutive(const ZBDDNode* zdd);


	static void printVector(std::vector<std::vector<int>*>& paths){
		for(auto it = paths.cbegin(); it != paths.cend(); ++it){
			//print out a path
			for(auto init = (*it)->cbegin(); init != (*it)->cend(); ++init){
				std::cout << *init << ", ";
			}
			std::cout << std::endl;

			//clear
			delete *it;
		}
		std::cout << std::endl;
	}

private:
	//fields
//	std::unordered_map<ZBDDNode_pointer, ZBDDNode*> unique_map;
	std::unordered_set<ZBDDNode_pointer> unique_set;

	std::unordered_set<const ZBDDNode*> marked_set;


	//private functions
//	const ZBDDNode* insert_base(const ZBDDNode* zdd);

	const ZBDDNode* subsumedDiff_rec(const ZBDDNode* f, const ZBDDNode* c);


	//for printing
	void printZDD_rec(const ZBDDNode* zdd);

	static void getPaths_rec(const ZBDDNode* zdd, std::vector<std::vector<int>*>& list, std::vector<int>* innerlist);
	static void getPaths_lengthlimit_rec(unsigned limit_length, const ZBDDNode* zdd, std::vector<std::vector<int>*>& list, std::vector<int>* innerlist);
	const ZBDDNode* createPaths_lengthlimit_rec(bool & flag, unsigned limit_ub, unsigned limit_lb, const ZBDDNode* zdd, unsigned length);

	//mark a node
	void markNode(const ZBDDNode* zdd);

	//test if a node is already marked
	bool isMarked(const ZBDDNode* zdd);

	//clear marked set
	void clearMarkedSet();

	//extract subset
	const ZBDDNode* extractSubset_rec(const ZBDDNode* zdd, long count, const ZBDDNode* parent, const ZBDDNode* prefix, const ZBDDNode* result);


};

//void ZBDDManager::printVector(std::vector<std::vector<int>>& paths) {
//	for(auto it = paths.cbegin(); it != paths.cend(); ++it){
//		//print out a path
//		for(auto init = (*it).cbegin(); init != (*it).cend(); ++init){
//			std::cout << *init << ", ";
//		}
//		std::cout << std::endl;
//	}
//	std::cout << std::endl;
//}


#endif /* SRC_ZBDDIMPL_ZBDDMANAGER_HPP_ */
