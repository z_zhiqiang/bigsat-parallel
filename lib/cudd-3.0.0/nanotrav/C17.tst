# Nanotrav Version #0.13, Release date 2015/7/15
# nanotrav/nanotrav -p 1 -cover ./nanotrav/C17.blif
# CUDD Version 3.0.0
Order before final reordering
1GAT(0) 2GAT(1) 3GAT(2) 6GAT(3) 7GAT(4) 
22GAT(10): 3 nodes 2 minterms
23GAT(9): 3 nodes 4 minterms
Equivalence failed at node 22GAT(10): 5 nodes 1 leaves 2 minterms
Equivalence failed at node 23GAT(9): 4 nodes 1 leaves 4 minterms
22GAT(10): 5 nodes 3 minterms
Testing iterator on ZDD paths:
-1-0- 1
-10-- 1
1-1-- 1

1-1-- 1
-10-- 1
-1-0- 1
23GAT(9): 5 nodes 3 minterms
Testing iterator on ZDD paths:
--0-1 1
-1-0- 1
-10-- 1

-10-- 1
-1-0- 1
--0-1 1
**** CUDD modifiable parameters ****
Hard limit for cache size: 2796202
Cache hit threshold for resizing: 30%
Garbage collection enabled: yes
Limit for fast unique table growth: 1677721
Maximum number of variables sifted per reordering: 1000
Maximum number of variable swaps per reordering: 2000000
Maximum growth while sifting a variable: 1.2
Dynamic reordering of BDDs enabled: no
Default BDD reordering method: 4
Dynamic reordering of ZDDs enabled: no
Default ZDD reordering method: 4
Realignment of ZDDs to BDDs enabled: yes
Realignment of BDDs to ZDDs enabled: no
Dead nodes counted in triggering reordering: no
Group checking criterion: 7
Recombination threshold: 0
Symmetry violation threshold: 10
Arc violation threshold: 10
GA population size: 0
Number of crossovers for GA: 0
Next reordering threshold: 4004
**** CUDD non-modifiable parameters ****
Memory in use: 3183048
Peak number of nodes: 1022
Peak number of live nodes: 20
Number of BDD variables: 5
Number of ZDD variables: 10
Number of cache entries: 32768
Number of cache look-ups: 113
Number of cache hits: 14
Number of cache insertions: 112
Number of cache collisions: 0
Number of cache deletions: 31
Cache used slots = 0.31% (expected 0.25%)
Soft limit for cache size: 16384
Number of buckets in unique table: 4096
Used buckets in unique table: 1.07% (expected 1.09%)
Number of BDD and ADD nodes: 24
Number of ZDD nodes: 21
Number of dead BDD and ADD nodes: 8
Number of dead ZDD nodes: 15
Total number of nodes allocated: 60
Total number of nodes reclaimed: 9
Garbage collections so far: 1
Time for garbage collection: 0.00 sec
Reorderings so far: 0
Time for reordering: 0.00 sec
Final size: 11
total time = 0.00 sec
Runtime Statistics
------------------
Machine name: tim-GP60-2PE
User time      0.0 seconds
System time    0.0 seconds

Average resident text size       =     0K
Average resident data+stack size =     0K
Maximum resident size            =  6088K

Virtual memory limit             = unlimited (unlimited)
Major page faults = 0
Minor page faults = 890
Swaps = 0
Input blocks = 8
Output blocks = 8
Context switch (voluntary) = 1
Context switch (involuntary) = 7
