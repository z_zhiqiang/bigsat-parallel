# Sat Solver Project
### Purpose
The  main purpose of this project is to make a scalable SAT solver. We observe that most of the solvers today (2017) are either sequential solvers or portfolio-based solvers. There is one distributed solver that we know (Dolius - http://www.cril.univ-artois.fr/articles/dolius14.pdf) which converts a CNF formula to multiple CNF formulas under one DNF formula. However, both the portfolio-based parallel solvers and this distributed solver are not fully scalable since they waste a lot of resources and hope that one worker solves the SAT problem quickly.

### What We've Tried
From 06/2016 to 09/2016, we found that the CDCL approach is really hard to scale because it was a tree path searching problem and we didn't know how big a subtree is. Hence, it was hard to do work balancing between the workers. That was the main reason why we tried to use the resolution approach instead. We used Apache Spark to test out the directional resolution algorithm (http://www.ics.uci.edu/~dechter/publications/R29a.pdf) and found that there are some buckets with crazy amount of clauses after the resolution process. This fact leads to memory issues and a decent amount of work unbalancing between workers.

From 10/2016 to 02/2017, we tried to focus on the performance and the memory issue first by creating a parallel solver instead of a distributed one. Since we want to have a good performance, we implemented the solver in C++ and created our own data structure to represent the clauses and buckets. Our proposed data structure did a decent job on reducing the memory for each bucket, but we still faced out-of-memory issue when we tried to solve harder SAT problems. Then, we found BDD and ZDD data structures. We implemented them to our SAT solver and solved the memory issue. However, the solver was not fast enough to beat the current state-of-the-art sequential solvers.

From 02/2017 to 09/2017, we dropped the project because we couldn't find any ways to beat other solvers. We also wanted to find other topics to work on.

### Current State
From early 10/2017 to mid 12/2017 (now), we found that other solvers worked really well on finding one possible solution, but they took a long time to count the total amount of solutions a SAT problem had. Since they were implemented in a path-searching way, it made senses that those solvers were having a hard time for solution counting. Hence, we tried to go back on our current SAT solver, but we focus on solution counting instead. For this time, we tried to use other existing bdd/zdd libraries out there. We found CUDD (sequential - http://vlsi.colorado.edu/~fabio/CUDD/cudd.pdf) and Sylvan (parallel - https://github.com/trolando/sylvan). Therefore, we focused on Sylvan first because we wanted to make our solver as scalable as possible. However, we were having troubles in Sylvan's poor documentation and complicated code/setup. Moreover, Sylvan didn't support ZDD operations which made our tasks a lot harder because we needed the ZDD product operation to do resolution. We couldn't find a way to transform that operation in BDD data structure. Therefore, we decided to drop the Sylvan library and moved to CUDD instead.

CUDD supported ZDD operations and had better documentations than Sylvan, but it didn't perform the operations in parallel mode with work stealing like Sylvan did. Hence, we decided to implement that ourselves in our solver. After we finished all the functions except the resolution process, we found that we needed to hack the CUDD code in order to eliminate all the clauses with both positive and negative literals of the same variable (ex: 1,-1,2,3; 2,-2,4,5; etc). It took us a while tinkering the CUDD code to find the right place. Here is a snippet on where we fixed the CUDD code (Note: you need to add this line to CUDD library in order to make the solver work)
```cpp
// cudd/CuddTable.c line 1374 function cuddUniqueInterZdd()
// add below code at the beginning of this function
if ((index % 2 == 1) && (T->index == index + 1)) {
  return E;
  }
```

After this problem was solved, we finished our demo sequential version with unit tests on those important functions related to CUDD. For now, all the components were done. We just needed to put everything together and fixed bugs (if exist) in our solver.

### Future Work
  - Put everything together to see if everything runs as expected
  - Implement solution counting function by backtracking (should be easy and possible since the solver uses resolution technique)
  - Parallelize CUDD operations within our solver
