#include <vector>
#include "../src/datastructure/CuddBuckets.hpp"

void initLog() {
  // init boost log
  boost::log::add_common_attributes();
  boost::log::core::get()->add_global_attribute("Scope", boost::log::attributes::named_scope());

  // change boost log format
  // format: [TimeStamp] [ThreadId] [Severity Level] [Scope] log message
  auto boostTimeFormat = boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%H:%M:%S");
  auto boostSeverityFormat = boost::log::expressions::attr<boost::log::trivial::severity_level>("Severity");
  boost::log::formatter logFmt = boost::log::expressions::format("[%1%] [%2%] %3%")
    % boostTimeFormat % boostSeverityFormat % boost::log::expressions::smessage;

  // console sink
  auto consoleSink = boost::log::add_console_log(std::clog);
  consoleSink->set_formatter(logFmt);
}

CuddBuckets testAddClause() {
  std::vector<Literal> literals1, literals2, literals3, 
                        literals4, literals5, literals6;
  literals1.push_back(Literal(-1));
  literals1.push_back(Literal(1));
  literals1.push_back(Literal(2));
  literals1.push_back(Literal(10));
  literals2.push_back(Literal(2));
  literals2.push_back(Literal(4));
  literals3.push_back(Literal(2));
  literals3.push_back(Literal(4));
  literals3.push_back(Literal(-5));
  literals3.push_back(Literal(10));
  literals4.push_back(Literal(1));
  literals4.push_back(Literal(10));
  literals4.push_back(Literal(15));
  literals5.push_back(Literal(-1));
  literals5.push_back(Literal(10));
  literals5.push_back(Literal(12));
  literals6.push_back(Literal(-1));
  literals6.push_back(Literal(2));
  literals6.push_back(Literal(10));
  literals6.push_back(Literal(11));
  Clause clause1(literals1), clause2(literals2), clause3(literals3), clause4(literals4),
         clause5(literals5), clause6(literals6);

  CuddBuckets bucket;
  bucket.addClause(clause1);
  bucket.addClause(clause2);
  bucket.addClause(clause3); 
  bucket.addClause(clause4);
  bucket.addClause(clause5);
  bucket.addClause(clause6);

  return bucket;
}

CuddBuckets testResolution() {
  CuddBuckets bucket = testAddClause();
  bucket.doResolutionPerBucket_Parallel(1);
  return bucket;
}

void testIsEmptyBucket() {
  CuddBuckets bucket = testAddClause();
  BOOST_LOG_TRIVIAL(info) << "isEmptyBucket(bucket1) = " 
      << (bucket.is_empty_bucket(1) ? "true" : "false")
      << " - expected = false";
  BOOST_LOG_TRIVIAL(info) << "isEmptyBucket(bucket3) = "
      << (bucket.is_empty_bucket(3) ? "true" : "false")
      << " - expected = true";
}

void testIsEmptyGenerated() {
  CuddBuckets bucket1 = testAddClause();
  BOOST_LOG_TRIVIAL(info) << "isEmptyGenerated(bucket1-1) = "
      << (bucket1.is_empty_generated(1) ? "true" : "false")
      << " - expected = false";
  BOOST_LOG_TRIVIAL(info) << "isEmptyGenerated(bucket1-2) = "
      << (bucket1.is_empty_generated(2) ? "true" : "false")
      << " - expected = false";

  std::vector<Literal> literals1, literals2;
  literals1.push_back(Literal(-1));
  literals2.push_back(Literal(1));
  Clause clause1(literals1), clause2(literals2);
  CuddBuckets bucket2;
  bucket2.addClause(clause1);
  bucket2.addClause(clause2);
  BOOST_LOG_TRIVIAL(info) << "isEmptyGenerated(bucket2-1) = "
      << (bucket2.is_empty_generated(1) ? "true" : "false")
      << " - expected = true";
  BOOST_LOG_TRIVIAL(info) << "isEmptyGenerated(bucket2-2) = "
      << (bucket2.is_empty_generated(2) ? "true" : "false")
      << " - expected = false";
}

int main() {
  initLog();

  BOOST_LOG_TRIVIAL(info) << "Test add clause";
  CuddBuckets bucketAddClause = testAddClause(); 
  bucketAddClause.printToFile("testAddClause.dot");
  BOOST_LOG_TRIVIAL(info) << "add clause result in testAddClause.png";

  BOOST_LOG_TRIVIAL(info) << "Test empty bucket";
  testIsEmptyBucket();
  std::cout << std::endl;

  BOOST_LOG_TRIVIAL(info) << "Test empty generated";
  testIsEmptyGenerated();
  std::cout << std::endl;

  BOOST_LOG_TRIVIAL(info) << "Test resolution";
  CuddBuckets bucketResolution = testResolution();
  bucketResolution.printToFile("testResolution.dot");
  BOOST_LOG_TRIVIAL(info) << "resolution result in testResolution.png"; 
}
