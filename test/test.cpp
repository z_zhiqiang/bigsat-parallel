//============================================================================
// Name        : ZBDD.cpp
// Author      : Zhiqiang Zuo
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "../src/common/BigSATCommon.hpp"
#include "../src/zbddimpl/ZBDDManager.hpp"
#include "../src/utility/ToString.hpp"

using namespace std;

//void printVector(vector<vector<int>>& paths);

//int main() {
//
//	vector<int> a1 = {2};
//	vector<int> a2 = {3, 5, 6};
//	vector<int> a3 = {1, 3};
//	vector<int> a4 = {2, 4};
//
//	ZBDDManager zddManager1;
//	const ZBDDNode* zdd1 = zddManager1.createPath(a1);
//	zddManager1.printZDD(zdd1);
//
//	ZBDDManager zddManager2;
//	const ZBDDNode* zdd2 = zddManager2.createPath(a2);
//	zddManager2.printZDD(zdd2);
////	ZBDDManager zddManager1;
//	const ZBDDNode* zddu12 = zddManager1.unionSubsumFree(zdd1, zdd2);
//	zddManager1.printZDD(zddu12);
//
//	ZBDDManager zddManager3;
//	const ZBDDNode* zdd3 = zddManager3.createPath(a3);
//	zddManager3.printZDD(zdd3);
//	ZBDDManager zddManager4;
//	const ZBDDNode* zdd4 = zddManager4.createPath(a4);
//	zddManager4.printZDD(zdd4);
////	ZBDDManager zddManager3;
//	const ZBDDNode* zddu34 = zddManager3.unionSubsumFree(zdd3, zdd4);
//	zddManager3.printZDD(zddu34);
//
//	ZBDDManager zddManager1234;
//	const ZBDDNode* zddp1234 = zddManager1234.productSTFree(zddu12, zddu34);
//	zddManager1234.printZDD(zddp1234);
//
//	std::vector<std::vector<int>*> pos_new_neg_new_vectors;
//	std::cout << "get path\n";
//	ZBDDManager::getPaths_lengthlimit(8, zddp1234, pos_new_neg_new_vectors);
//	std::cout << "get path done\n";
//	std::cout << pos_new_neg_new_vectors.size() << std::endl;
//	ToString::printOutInputs(pos_new_neg_new_vectors);
//	pos_new_neg_new_vectors.clear();
//
//	ZBDDManager zddManager;
//	std::cout << "get path\n";
//	bool flag = false;
//	const ZBDDNode* fn = zddManager.createPaths_lengthlimit(flag, 2, 0, zddp1234);
//	std::cout << flag << std::endl;
//	std::cout << "get path done\n";
////	std::cout << pos_new_neg_new_vectors.size() << std::endl;
////	ToString::printOutInputs(pos_new_neg_new_vectors);
//	zddManager.printZDD(fn);
//
////	int myints[] = {-32,71,12,-12,-26,80,-53,33};
////	std::vector<int> myvector (myints, myints+8);
////	std::sort(myvector.begin(), myvector.end(), [](int r, int l){
////	    int abs1 = std::abs(r);
////	    int abs2 = std::abs(l);
////	    return (abs1 == abs2) ? (r < l) : (abs1 < abs2);
////	});
////	for(auto it = myvector.cbegin(); it != myvector.cend(); ++it){
////		std::cout << *it << ",";
////	}
////	std::cout << std::endl;
//
//}

//void printVector(vector<vector<int>>& paths) {
//	for(auto it = paths.cbegin(); it != paths.cend(); ++it){
//		//print out a path
//		for(auto init = (*it).cbegin(); init != (*it).cend(); ++init){
//			cout << *init << ", ";
//		}
//		cout << endl;
//	}
//	cout << endl;
//}
