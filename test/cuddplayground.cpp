#include <stdio.h>
#include <iostream>
#include "cudd.h" 

void printToFile(const char* filename, DdManager* manager, DdNode* root) {
  DdNode** toPrint = new DdNode*[1];
  toPrint[0] = root;

  // output to the file
  FILE* file = fopen(filename, "w");
  Cudd_zddDumpDot(manager, 1, toPrint, NULL, NULL, file);
  fclose(file);
  delete[] toPrint;
}


int main() {
  DdManager* manager = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);

  // init nodes 
  DdNode* f = Cudd_ReadZddOne(manager, 0);
  Cudd_Ref(f);
  for (int i = 5; i < 10; ++i) {
    f = Cudd_zddChange(manager, f, i);
  }
  printToFile("testcudd1.dot", manager, f);
  printToFile("testcudd2.dot", manager, Cudd_T(f));
  printToFile("testcudd3.dot", manager, Cudd_zddIthVar(manager, 7));

  std::cout << Cudd_NodeReadIndex(f) << std::endl;

  if (!manager) {
    Cudd_Quit(manager);
  }
}
